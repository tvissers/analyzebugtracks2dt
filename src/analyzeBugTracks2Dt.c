/*
    This file is part of analyzeBugTracks2Dt.

    analyzeBugTracks2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    analyzeBugTracks2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with analyzeBugTracks2Dt. If not, see <http://www.gnu.org/licenses/>.
*/

/* About this program

analyzeBugTracks2Dt is designed to analyse and classify trajectories of bacteria on a surface. 
The code is written by Teun Vissers. Please cite [T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018] if you use this program for a scientific publication.

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <glob.h>

#define NOBINS 30
#define NOBINSRESERVE 31

/* Variables: Global variables indicated with CAPITAL LETTERS
  - Globals obtained from trajectory input file:

  N_TRACKS - Number of trajectories
  N_FRAMES - Number of frames

  - Number of trajectories of different types, see giveKind for exact definitions:

  N_SWIMMERS - Number of swimmers, visible using short k_T
  N_DIFFUSERS1 - Number of diffusers type 1 (duration < MINANB), visible using short k_T
  N_DIFFUSERS2 - Number of diffusers type 2 (duration > MINANB), visible using short k_T
  // Adherers per definition have long trajectories (duration > MINANB):
  N_WOBBLERS - Number of wobblers, visible using short k_T and short k_R
  N_PIVOTERS1 -  Number of pivoters type 1, visible using short k_T and short k_R
  N_PIVOTERS2 - Number of pivoters type 2, visible using long k_T and short k_R
  N_ROTATORS1 - Number of active rotators type 1, visible using short k_T and short k_R 
  N_ROTATORS2 - Number of active rotators type 2, visible using long k_T and short k_R
  N_AMBIGUOUS - Remaining trajectories..
  N_UNDEFINED - Trajectories too short to classify (duration < MINANA)

  - These arrays show the number of each type for each frame:

  SWIMMER_ARRAY - list containing number of swimmers per frame;;
  DIFFUSER1_ARRAY - list containing number of diffusers type 1 per frame;
  DIFFUSER2_ARRAY - list containing number of diffusers type 2 per frame;;
  WOBBLER_ARRAY - list containing number of wobblers per frame;;
  PIVOTER1_ARRAY - list containing number of pivoters type 1 per frame;;
  PIVOTER2_ARRAY - list containing number of pivoters type 2 per frame;;
  ROTATOR1_ARRAY - list containing number of rotators type 1 per frame;;
  ROTATOR2_ARRAY - list containing number of rotators type 2 per frame;;
  AMBIGUOUS_ARRAY - list containing number of ambiguous per frame;;
  UNDEFINED_ARRAY - list containing number of undefined per frame;;

  - Parameters:read from input file:

  DEBUG - Sets the amount of checks and DEBUG output that is generated.
          0 is minimal (no output), higher numbers will generate more output 
  TRACKINPUTFILE - Input file containing trajectories
  OUTPUTDIR - Output directory
  MINANA - Minimum trajectory length required for classification
  MINANB - Minimum trajectory length of adherers
  TAUSHORT - Short time-scale
  TAULONG - Long time-scale
  PIXELSIZE - Size of one pixel
  TIMESTEP - Time in between two consecutive frames
  WRITETRACKINFO - Set to 1 to export information of individual trajectories (can be lots of files!), to 0 if not
  SPAN - Used for smoothening    
  LOWERBOUNDSHORTKT - Lower boundary for short-term translational exponent used for classification
  LOWERBOUNDLONGKT - Lower boundary for long-term translational exponent used for classification
  UPPERBOUNDSHORTKT - Upper boundary for short-term translational exponent used for classification
  LOWERBOUNDSHORTKR - Lower boundary for short-term rotational exponent used for classification
  UPPERBOUNDSHORTKR - Upper boundary for short-term rotational exponent used for classification

*/

int N_TRACKS;
int N_FRAMES;

int N_ROTATORS2 = 0;
int N_SWIMMERS = 0;
int N_WOBBLERS = 0;
int N_DIFFUSERS2 = 0;
int N_DIFFUSERS1 = 0;
int N_AMBIGUOUS = 0;
int N_PIVOTERS2 = 0;
int N_PIVOTERS1 = 0;
int N_ROTATORS1 = 0;
int N_UNDEFINED = 0;

int* ROTATOR1_ARRAY;
int* ROTATOR2_ARRAY;
int* SWIMMER_ARRAY;
int* WOBBLER_ARRAY;
int* DIFFUSER1_ARRAY;
int* DIFFUSER2_ARRAY;
int* AMBIGUOUS_ARRAY;
int* PIVOTER1_ARRAY;
int* PIVOTER2_ARRAY;
int* UNDEFINED_ARRAY;

int DEBUG=10;
char TRACKINPUTFILE[1000]="";
char OUTPUTDIR[1000]="";
int MINANA = 5000; 
int MINANB = 10000;
int TAUSHORT = 0;
int TAULONG = 0;
float PIXELSIZE = 0.0;
float TIMESTEP = 0.0;
int WRITETRACKINFO = 1;
int SPAN = 0;
float LOWERBOUNDSHORTKT = 0.6;
float LOWERBOUNDLONGKT = 0.6;
float UPPERBOUNDSHORTKT = 1.3;
float LOWERBOUNDSHORTKR = 0.3;
float UPPERBOUNDSHORTKR = 1.2;

float ADDED_ERROR = 0.0;
//float ADDED_ERROR = 0.01;

#include "namelist.h"

NameList nameList[] = {
  NameI (WRITETRACKINFO),
  NameC (OUTPUTDIR),            
  NameC (TRACKINPUTFILE),
  NameI (DEBUG),             
  NameR (PIXELSIZE),
  NameR (TIMESTEP),
  NameI (MINANA),            
  NameI (MINANB),             
  NameI (TAUSHORT),
  NameI (TAULONG),
  NameR (LOWERBOUNDSHORTKT),
  NameR (LOWERBOUNDLONGKT),
  NameR (UPPERBOUNDSHORTKT),
  NameR (LOWERBOUNDSHORTKR),
  NameR (UPPERBOUNDSHORTKR),
  NameI (SPAN),
};

#include "namelist.c"

typedef struct 
{
  int x;
  int y;
} tVectori;

tVector** trackTable;
int** trackDetails;

typedef struct 
{
	/* Variables: Fields inside the structure tTrackDetails 
	   trackDuration - duration of the trajectory (in number of frames)
	*/
	int trackDuration;
	int firstFrame;
	double rot_average;
	double rot_abs_average;
	double rot_average2;
	double rot_max_average;
	double rot_max_abs_average;
	double rot_max_average2;
	int tau_short;
	int tau_long;
	double theta;
	double average_rodlength;
	double max_rodlength;
	double msd_short_list[NOBINSRESERVE];
	double msd_long_list[NOBINSRESERVE];
	double trans_list[NOBINSRESERVE];
	double peak_trans_list[NOBINSRESERVE];
	double min_trans_list[NOBINSRESERVE];
	double* tau_array_trans_slopes;
	double* tau_array_log_trans_slopes;
	double* tau_array_rot_slopes;
	double* tau_array_log_rot_slopes;
	double mostfirm_long_msd_value;
	double mostfirm_long_msd_coordinate;
	double leastfirm_long_msd_value;
	double leastfirm_long_msd_coordinate;
	double mostfirm_short_msd_value;
	double mostfirm_short_msd_coordinate;
	double leastfirm_short_msd_value;
	double leastfirm_short_msd_coordinate;
	double rotation_std_max;
	double long_rotation_slope;
	double long_rotational_exponent;
	double short_rotation_slope;
	double short_rotational_exponent;
	double short_centre_translation_slope;
	double short_centre_translational_exponent;
	double long_centre_translation_slope;
	double long_centre_translational_exponent;
	double track_var;
	double track_var_norm;
	double short_anchor_translation_slope;
	double short_anchor_translational_exponent;
	double long_anchor_translation_slope;
	double long_anchor_translational_exponent;
	double timewise_mostfirm_msd_value; // used for rotators
	double timewise_mostfirm_msd_timepoint; // used for rotators
	double *tau_values;
	double *msd_values_for_tau;
	double *msd_xvalues_for_tau;
	double *msd_yvalues_for_tau;
	int *msd_count_for_tau;
	bacType thisType;
} tTrackDetails;

typedef struct 
{
	int x;
	int y;
} tIntVector;

tTrackDetails* allTrackDetails;

double rc_frameWidth=1040.0*3;
double rc_frameHeight=1040.0*3;

int frame; //frame number for timeseries

void mygetline(char* str,FILE *f)
{
	int comment=1;
	while(comment)
	{
		if (!fgets(str,256,f)) return;
		if(str[0]!='#') comment=0;
	}
}

/* Function: dot
        Calculate dot product between a and b

        Parameters:
                a - vector
                b - vector

        Returns:
                dot product between a and b
*/
inline double dot(dir a, dir b)
{
  	return a.x*b.x+a.y*b.y;
}

/* Function: cross
        Calculate cross product between M and N

        Parameters:
                M - vector
                N - vector

        Returns:
                cross product between M and N
*/
double cross(dir M, dir N)
{
        double z;
        z = M.x*N.y-M.y*N.x;
        return z;
}

/* Function: normalizeVector
	Normalises a vector with x and y coordinate

        Parameters:
		v - vector to normalise

        Returns:
                normalised vector
*/
dir normalizeVector(dir v)
{
        double norm = sqrt(v.x*v.x+v.y*v.y);
	if (norm > 0)
	{
        	v.x = v.x/norm;
	        v.y = v.y/norm;
	}
	else
	{
		v.x = 0.0;
		v.y = 0.0;
	}
        return v;
}

/* Function: calcAngle
        Determines the angle between orientations j and k for a cell in trajectory i

        Parameters:
                i - number of the trajectory
                j - frame number
                k - frame number

        Returns:
                angle between orientations in frames j and k
*/
double calcAngle(int i, int j, int k)
{
	// this function calculates the angle from start to stop. If start=stop, it will calculate it for one time-interval

        dir Q;
        dir R;

        double this_rot = 0.0;

        Q.x  = trackTable[i][j].normalized_orientation.x;
        Q.y  = trackTable[i][j].normalized_orientation.y;

        R.x  = trackTable[i][k].normalized_orientation.x;
        R.y  = trackTable[i][k].normalized_orientation.y;

        double d1 = dot(Q,R);
        if (d1 > 1.0) // due to rounding errors this can be e.g. 1.000000001, which would cause acos to go wrong later. We prevent that by constraining the boundaries:
	        d1 = 1.0;
        else if (d1 < -1.0)
        	d1 = -1.0;

        double angle = acos(d1); // result of acos is always positive
        double sign = cross(Q,R); // used to determine clockwise or counterclockwise in-plane rotation

        if (sign > 0)
        	this_rot += 1*angle; // clockwise (or counterclockwise, depending how you look at it)
        else
                this_rot -= 1*angle; // counterclockwise (or clockwise, depending how you look at it)
        
        return this_rot;
}

/* Function: getTranslationSlope
        Determines the translational exponent k_T and other information for a trajectory

        Parameters:
                i - number of the trajectory
                s_T - details of a trajectory
                tau_short - short time scale over which k_T is determined
                tau_long - long time scale over which k_T is determined
                coordinate - the coordinate along the length axis of the cell in this trajectory for which to calculate stuff
        Returns:
                calculated values        
*/
double* getTranslationSlope(int i, tTrackDetails* s_T, int tau_short, int tau_long, double coordinate)
{
	int* counters= (int*) malloc(sizeof(int)*s_T->trackDuration);
        double* msd2= (double*) malloc(sizeof(double)*s_T->trackDuration); 
	double* result= (double*) malloc(sizeof(double)*7);
	double* tau_slopes= (double*) malloc(sizeof(double)*s_T->trackDuration);
	double* tau_log_slopes= (double*) malloc(sizeof(double)*s_T->trackDuration);
	double dt = TIMESTEP;
	
        int mmm = 0;
        for (mmm = 0; mmm < s_T->trackDuration; mmm++)
        {
                counters[mmm] = 0;
                msd2[mmm] = 0.0;
		tau_slopes[mmm] = 0.0;
                tau_log_slopes[mmm] = 0.0;
        }

	int delta = 0;
	
        for (delta = 1; delta < s_T->trackDuration; delta++)
        {
                 double msd_now=0.0;
                 int Nn=0;
                 int kk = 0;
                 for (kk = 0; kk < s_T->trackDuration-delta; kk++)
                 {
                        Nn++;

                        dir point1;
                        dir point2;
                        
			if (coordinate == -1.0)
			{
				point1.x = trackTable[i][delta+kk].x;
				point1.y = trackTable[i][delta+kk].y;
				point2.x = trackTable[i][kk].x;
				point2.y = trackTable[i][kk].y;
			}
			else
			{
				dir extreme1;
				dir extreme2;

				dir normalizedOrientation1;
				normalizedOrientation1.x=trackTable[i][delta+kk].normalized_orientation.x;
				normalizedOrientation1.y=trackTable[i][delta+kk].normalized_orientation.y;

				dir normalizedOrientation2;
				normalizedOrientation2.x=trackTable[i][kk].normalized_orientation.x;
				normalizedOrientation2.y=trackTable[i][kk].normalized_orientation.y;

				extreme1.x =  trackTable[i][delta+kk].x - 0.5*normalizedOrientation1.x*trackTable[i][delta+kk].length; // left pole coordinate
				extreme1.y =  trackTable[i][delta+kk].y - 0.5*normalizedOrientation1.y*trackTable[i][delta+kk].length; // left pole coordinate
				point1.x = extreme1.x + coordinate*normalizedOrientation1.x*trackTable[i][delta+kk].length;
				point1.y = extreme1.y + coordinate*normalizedOrientation1.y*trackTable[i][delta+kk].length;

				extreme2.x =  trackTable[i][kk].x - 0.5*normalizedOrientation2.x*trackTable[i][kk].length; // left pole coordinate
				extreme2.y =  trackTable[i][kk].y - 0.5*normalizedOrientation2.y*trackTable[i][kk].length; // left pole coordinate
				point2.x = extreme2.x + coordinate*normalizedOrientation2.x*trackTable[i][kk].length; // point corresponding to coordinate on length axis
				point2.y = extreme2.y + coordinate*normalizedOrientation2.y*trackTable[i][kk].length; // point corresponding to coordinate on length axis
			}

                        msd_now += (point2.x-point1.x)*(point2.x-point1.x)+(point2.y-point1.y)*(point2.y-point1.y);
                 }		 
		 //msd2[delta] = msd_now*PIXELSIZE*PIXELSIZE/(double)Nn+ADDED_ERROR;  
		 msd2[delta] = msd_now*PIXELSIZE*PIXELSIZE/(double)Nn;  
         }

	 int j = 0;

	 // this part calculates the time-scale tau for which msd is the smallest 
	 if (s_T->trackDuration > MINANB)
	 {
		double smallest = msd2[tau_long];
		result[2] = smallest;
		result[3] = tau_long*dt;
		for (j = 1; j < tau_long; j++)
         	{
			if (msd2[j] < smallest)
			{
				smallest = msd2[j];
				result[2]= smallest;
				result[3]= j*dt;
			}
	        }	
	 }
	 else
	 {
		result[2] = 0.0;
		result[3] = 0.0;
         }
	 
	 result[0] = 0.0;
	 result[1] = 0.0;
	 result[4] = 0.0;
	 result[5] = 0.0;

	 for (j = 2; j < s_T->trackDuration; j++)
         {
		// these values are written to an output file at the end
		tau_slopes[j] = (msd2[j]-msd2[1])*(1.0/( (j*dt)-(1*dt)));
		tau_log_slopes[j] = (log(msd2[j])-log(msd2[1]))*(1.0/(log(j*dt)-log((1)*dt))); // translational exponents for different time-scales tau
	 }
	
	 // short-term translational exponent, used to classify trajectories.
	 result[0] = (msd2[tau_short]-msd2[1])*(1.0/( (tau_short*dt)-(1*dt)));
         result[1] = (log(msd2[tau_short])-log(msd2[1]))*(1.0/(log(tau_short*dt)-log((1)*dt))); // used in giveKind
	 //printf("%i %f %f\n", tau_short, msd2[1], dt);
 
	 if (s_T->trackDuration > MINANB)
         {
	 	// long-term translational exponent, used to classify some trajectories.
		 result[4] = (msd2[tau_long]-msd2[1])*(1.0/( (tau_long*dt)-(1*dt)));
        	 result[5] = (log(msd2[tau_long])-log(msd2[1]))*(1.0/(log(tau_long*dt)-log((1)*dt))); // used in giveKind
	 }

	 s_T->tau_array_trans_slopes = tau_slopes;
	 s_T->tau_array_log_trans_slopes = tau_log_slopes;

         free(msd2);
	 free(counters);

	 return result;
}

/* Function: getRotationSlope
        Determines the rotational exponent k_R and other information for a trajectory

        Parameters:
                i - number of the trajectory
                s_T - details of a trajectory
                tau_short - short time scale over which k_R is determined
                tau_long - long time scale over which k_R is determined

        Returns:
        	calculated values        
*/
double* getRotationSlope(int i, tTrackDetails* s_T, int tau_short, int tau_long)
{
        int* counters= (int*) malloc(sizeof(int)*s_T->trackDuration);
        double* msd_or= (double*) malloc(sizeof(double)*s_T->trackDuration);
        double* msd_or2= (double*) malloc(sizeof(double)*s_T->trackDuration);
        double* msd_abs_or= (double*) malloc(sizeof(double)*s_T->trackDuration);
	double* tau_rot_slopes= (double*) malloc(sizeof(double)*s_T->trackDuration);
	double* tau_log_rot_slopes= (double*) malloc(sizeof(double)*s_T->trackDuration);
	double dt = TIMESTEP;

        int mmm = 0;
        for (mmm = 0; mmm < s_T->trackDuration; mmm++)
        {
                counters[mmm] = 0;
                msd_or[mmm] = 0.0;
                msd_or2[mmm] = 0.0;
                msd_abs_or[mmm] = 0.0;
		tau_rot_slopes[mmm] = 0.0;
                tau_log_rot_slopes[mmm] = 0.0;
        }

        int j = 0;
        int kkk = 0;
	
        for (kkk = 0; kkk < s_T->trackDuration-1; kkk++) // starting point of the track 
        {
                double tot_angle = 0.0;
                int dist = 0;
                for(j=kkk; j < (s_T->trackDuration-1); j++) // start going from starting point to end point, effectively meeting different distances
                {
                        dist++;
			tot_angle += calcAngle(i, j, j+1);
                        msd_or[dist] += tot_angle;
                        msd_abs_or[dist] += fabs(tot_angle);
                        msd_or2[dist] += tot_angle*tot_angle;
                        counters[dist] += 1;
                }
        }

        for (j = 0; j < s_T->trackDuration-1; j++)
        {
                if (counters[j] > 0 && j > 0)
                {
                        msd_or[j] = msd_or[j]/( (double)counters[j] );
			msd_abs_or[j] = msd_abs_or[j]/( (double)counters[j] );
			msd_or2[j] = msd_or2[j]/((double)counters[j] );
                }
        }

        double* result= (double*) malloc(sizeof(double)*4);
	result[0] = 0.0;
	result[1] = 0.0;

	result[2] = 0.0;
	result[3] = 0.0;

	int cc = 0;
	int dd = 0;

        double dt_inv = 1.0/dt;

	for (j = 2; j < s_T->trackDuration; j++)
        {
                tau_rot_slopes[j] = (msd_or2[j]-msd_or2[1])*(1.0/( (j*dt)-(1*dt)));
                tau_log_rot_slopes[j] = (log(msd_or2[j])-log(msd_or2[1]))*(1.0/(log(j*dt)-log((1)*dt)));
        }

	// short-time rotational exponent:
	result[2] = (msd_or2[tau_short]-msd_or2[1])*(1.0/( (tau_short*dt)-(1*dt)));
        result[3] = (log(msd_or2[tau_short])-log(msd_or2[1]))*(1.0/(log(tau_short*dt)-log((1)*dt))); // used in giveKind
	
	if (s_T->trackDuration >= MINANB)
	{
		// long-time rotational exponent:
		 result[0] = (msd_or2[tau_long]-msd_or2[1])*(1.0/( (tau_long*dt)-(1*dt)));
	         result[1] = (log(msd_or2[tau_long])-log(msd_or2[1]))*(1.0/(log(tau_long*dt)-log((1)*dt)));
	}

	cc = 0;
	dd = 0;
	double E2 = 0.0;
	double RE = 0.0;
	double dT=0.0;

	if (s_T->trackDuration >= MINANB)
        {
		s_T->rot_max_average = msd_or[tau_long];
		s_T->rot_max_abs_average = msd_abs_or[tau_long];
		s_T->rot_max_average2 = msd_or2[tau_long];
		
		int statMax = tau_long;

		int lpp=0;
		for (lpp = 2; lpp < statMax && lpp < s_T->trackDuration; lpp++)
		{
			if ( counters[lpp] > 0 && counters[lpp-1] > 0)
			{
				cc++;
				dT+=dt;
				double value = (msd_or2[lpp]-msd_or2[lpp-1]);

				if (lpp > 0.5*statMax)
				{
					dd++;
					RE += value*dt_inv;
					E2 += value*value*dt_inv*dt_inv; 
				}
			}
        	}
		
		RE = RE / (double)dd;
		E2 = E2 / (double)dd;
	}
	else
	{
		RE = 0.0;
		E2 = 0.0;
	}
	
	double variance = E2-(RE*RE);

	s_T->rot_average = msd_or[tau_short];
	s_T->rot_abs_average = msd_abs_or[tau_short];
	s_T->rot_average2 = msd_or2[tau_short];

	s_T->tau_array_rot_slopes = tau_rot_slopes;
	s_T->tau_array_log_rot_slopes = tau_log_rot_slopes;

	s_T->rotation_std_max = sqrt(s_T->rot_max_average2-s_T->rot_max_average*s_T->rot_max_average);
	s_T->track_var = variance; // normalise variance by mean value
	s_T->track_var_norm = variance / fabs(result[0]); // normalise variance by mean value

	free(counters);
        free(msd_or);
        free(msd_abs_or);
        free(msd_or2);

        return result;
}

/* Function: giveKind
        Determines the identity of the trajectory

        Parameters:
		s_T - details of a trajectory

	Returns:
		the type of the trajectory
*/
bacType giveKind(tTrackDetails s_T)
{
        int trackDuration = s_T.trackDuration;

	double short_rotation_exponent = s_T.short_rotational_exponent;
	double short_anchor_translation_exponent = s_T.short_anchor_translational_exponent;
	double long_anchor_translation_exponent = s_T.long_anchor_translational_exponent;
	
        bacType thisType=undefined;
       
	double lowT = LOWERBOUNDSHORTKT;
	double long_lowT = LOWERBOUNDLONGKT;
	double highT = UPPERBOUNDSHORTKT;
	double lowR = LOWERBOUNDSHORTKR;
	double highR = UPPERBOUNDSHORTKR;
 
	if (trackDuration >= MINANA)
        {
		if (trackDuration < MINANB) // short trajectories, but long enough to classify
        	{
			if ( short_anchor_translation_exponent >= highT )
			{
				// swimmer because of high translational exponent
                	        thisType = swimmer;
			}
			else if ( (short_anchor_translation_exponent >= lowT && short_anchor_translation_exponent < highT) )
			{
				// typical translational exponent for a diffuser
				thisType = diffuser1;
			}
			else
			{
				// shows translational exponent typical for adherer, but adherers typically have long trajectories
				// hence, this might be a diffuser in disguise..
				thisType = ambiguous;
			}
		}
		else if (trackDuration >= MINANB) // longer trajectories.
                {
			if ( short_anchor_translation_exponent >= highT )
			{
				// swimmer because of high translational exponent
                                thisType = swimmer;
			}
			else if (short_anchor_translation_exponent < lowT && short_rotation_exponent < lowR)  
			{
				// adherer classified as wobbler, low translational exponent and low rotational exponent
				thisType = wobbler;
			}
			else if (short_anchor_translation_exponent < lowT && (short_rotation_exponent >= lowR && short_rotation_exponent < highR) )
			{
				// adherer classified as pivoter, low translational exponent and mid-range rotational exponent
				thisType = pivoter1;
			}
			else if (long_anchor_translation_exponent < long_lowT && (short_rotation_exponent >= lowR && short_rotation_exponent < highR) ) 
			{
				// use longer time translational exponent to detect pivoting cells swaying out of plane, that would otherwise be 
				// erroneously identified as diffusers
                                thisType = pivoter2;
			}
			else if (short_anchor_translation_exponent < lowT && short_rotation_exponent >= highR)
			{ 
				// adherer classified as active rotator, low translational exponent and high rotational exponent
                        	thisType = rotator1;
			}
			else if (long_anchor_translation_exponent < long_lowT && short_rotation_exponent >= highR) // spinning and stuck, using longer time scale
			{
				// use longer time translational exponent to detect actively rotating cells swaying out of plane, that would otherwise be 
				// erroneously identified as diffusers
                                thisType = rotator2;
			}
        	        else if (short_anchor_translation_exponent >= lowT && short_anchor_translation_exponent < highT)
                                thisType = diffuser2;
                        else
                        {
				// this should never happen, as trajectories should be in one of the categories above.
                                printf("Trajectory with long duration not properly classified!\n");
				exit(666);
                        }
                }
        }
        else // trajectory to short to be classified
                thisType = undefined;

        return thisType;
}

/* Function: classify
        Classifies this trajectory, and updates the books. Called by preprocessing routine preprocess_tracks().

        Parameters:
		s_T - details of a trajectory
*/
void classify(tTrackDetails s_T)
{
		
	bacType thisType = undefined;
	thisType = giveKind(s_T);

	if (thisType == wobbler)
		N_WOBBLERS++;
	else if (thisType == diffuser1)
		N_DIFFUSERS1++;
	else if (thisType == diffuser2)
		N_DIFFUSERS2++;
	else if (thisType == swimmer)
		N_SWIMMERS++;
	else if (thisType == rotator2)
		N_ROTATORS2++;
	else if (thisType == ambiguous)
		N_AMBIGUOUS++;
	else if (thisType == pivoter2)
		N_PIVOTERS2++;
	else if (thisType == pivoter1)
		N_PIVOTERS1++;
	else if (thisType == rotator1)
		N_ROTATORS1++;
	else if (thisType == undefined)
		N_UNDEFINED++;
}

/* Function: addToTable
        Add to the books for this frame a cell of a certain type

        Parameters: 
                bacType - type of trajectory (diffuser, swimmer, ..)
                frame - frame number
*/
void addToTable(bacType thisType, int frame)
{
	if (thisType == rotator2)
		ROTATOR2_ARRAY[frame]++;
	else if (thisType == swimmer)
		SWIMMER_ARRAY[frame]++; 
	else if (thisType == diffuser2)
		DIFFUSER2_ARRAY[frame]++;
	else if (thisType == diffuser1)
		DIFFUSER1_ARRAY[frame]++;
	else if (thisType == wobbler)
		WOBBLER_ARRAY[frame]++;
	else if (thisType == ambiguous)
		AMBIGUOUS_ARRAY[frame]++;
	else if (thisType == pivoter2)
		PIVOTER2_ARRAY[frame]++;
	else if (thisType == pivoter1)
		PIVOTER1_ARRAY[frame]++;
	else if (thisType == rotator1)
		ROTATOR1_ARRAY[frame]++;
	else if (thisType == undefined)
		UNDEFINED_ARRAY[frame]++;
}

/* Function: writeTrackToFile
        Append a trajectory to a file containing all trajectories

        Parameters: 
                thisType - type of trajectory (diffuser, swimmer, ..)
                trackTable - table containing all trajectories
                s_T - details of trajectory i
                i - number of the trajecory
*/
void writeTrackToFile(bacType thisType, tVector **trackTable, tTrackDetails s_T, int i)
{
	int trackDuration = s_T.trackDuration;
	int firstFrame = s_T.firstFrame;

	char buffer[1024];
	char register_adhering[1024];
	 
	snprintf(buffer, sizeof(buffer), "%stracks_ALLCATS.dat", OUTPUTDIR);
	FILE* fp = fopen(buffer, "a");
	
	snprintf(register_adhering, sizeof(register_adhering), "%sregister_adhering.dat", OUTPUTDIR);

	int cat = 0;
	if (thisType == rotator2)
		cat = 1;	
         else if (thisType == swimmer)
		cat = 2;
         else if (thisType == wobbler)
		cat = 3;
         else if (thisType == diffuser2)
		cat = 4;
         else if (thisType == diffuser1)
		cat = 5;	
         else if (thisType == ambiguous)
		cat = 6;
         else if (thisType == pivoter2)
		cat = 7;
         else if (thisType == pivoter1)
		cat = 8;
         else if (thisType == rotator1)
		cat = 9;
         else if (thisType == undefined)
		cat = 10;

	 if (cat == 1 || cat == 3 || cat == 7 || cat == 8 || cat == 9) // adhering cells can go into this register
	 {
		 FILE* fpg = fopen(register_adhering, "a");
		 double counts = 0.0;
		 double x_av = 0.0;
		 double y_av = 0.0;
		 int j = 0;
		 for(j=0; j < trackDuration; j++) // start going through track i
		 {
			dir extreme;
			dir cen;
			extreme.x=trackTable[i][j].x-0.5*trackTable[i][j].normalized_orientation.x*trackTable[i][j].length;
                        extreme.y=trackTable[i][j].y-0.5*trackTable[i][j].normalized_orientation.y*trackTable[i][j].length;
                        cen.x=extreme.x+trackTable[i][j].normalized_orientation.x*s_T.mostfirm_short_msd_coordinate*trackTable[i][j].length; // point that moves least. FIX (minor): 21-10-2017    
                        cen.y=extreme.y+trackTable[i][j].normalized_orientation.y*s_T.mostfirm_short_msd_coordinate*trackTable[i][j].length;
			x_av += cen.x;
			y_av += cen.y;
			counts++;
		 }
		 x_av = x_av/counts;
		 y_av = y_av/counts;
		 fprintf(fpg, "%i %i %i %i %.12f %.12f %.12f %.12f\n", i, cat, firstFrame, trackDuration, x_av, y_av, s_T.mostfirm_short_msd_coordinate, s_T.leastfirm_short_msd_coordinate);
		 fclose(fpg);
	 }

	 fprintf(fp, "&%i &%i &%i &%.12f\n", i, trackDuration, cat, s_T.rot_max_average2);
	 int j = 0;
	 for(j=0; j < trackDuration; j++) // start going through track i
	 {
		if (trackTable[i][j].x == 0 || trackTable[i][j].y == 0)
			 printf("WRONG TRACK!\n");

		dir normalizedOrientation;
		normalizedOrientation.x=trackTable[i][j].normalized_orientation.x;
		normalizedOrientation.y=trackTable[i][j].normalized_orientation.y;

		fprintf(fp, "%i %i %f %f %f %f %f %f %f %f\n", firstFrame+j, trackTable[i][j].n, trackTable[i][j].x, trackTable[i][j].y, normalizedOrientation.x, normalizedOrientation.y, trackTable[i][j].length, s_T.mostfirm_short_msd_coordinate, s_T.leastfirm_short_msd_coordinate, trackTable[i][j].swimmingDirection_cor); // changed 18 may 2017
	 }
	fclose(fp);
}

/* Function: calculateTranslationalDiffusionInTime
        Calculates the MSD as function of time-interval to a file for a trajectory and stores it in memory

        Parameters: 
                s_T - details of trajectory i
                i - number of the trajecory
                thisType - type of trajectory (diffuser, swimmer, ..)
                mode - select from options: 0 = for anchoring point, 1 is for point of maximum movement, 2 is for centre of mass.
*/
void calculateTranslationalDiffusionInTime(tTrackDetails *s_T, int i, int mode)
{
	double dt = TIMESTEP;

	double fraction = 0.5;
        if (mode == 0)
                fraction = s_T->mostfirm_short_msd_coordinate; // plotting MSD of most anchored point. FIX (minor): 21-10-2017
        if (mode == 1)
                fraction = s_T->leastfirm_short_msd_coordinate; // plotting MSD of least anchored point.
        if (mode == 2)
                fraction = 0.5; // COM.

	s_T->tau_values = (double*) malloc(sizeof(double)*(s_T->trackDuration));
	s_T->msd_values_for_tau = (double*) malloc(sizeof(double)*(s_T->trackDuration));
	s_T->msd_xvalues_for_tau = (double*) malloc(sizeof(double)*(s_T->trackDuration));
	s_T->msd_yvalues_for_tau = (double*) malloc(sizeof(double)*(s_T->trackDuration));
	s_T->msd_count_for_tau = (int*) malloc(sizeof(int)*(s_T->trackDuration));

	s_T->tau_values[0] = 0.0;
        s_T->msd_values_for_tau[0] = 0.0;
        s_T->msd_xvalues_for_tau[0] = 0.0;
        s_T->msd_yvalues_for_tau[0] = 0.0;
        s_T->msd_count_for_tau[0] = 0;

        int delta = 0;
        for (delta = 1; delta < s_T->trackDuration; delta++)
        {
                 double msd_now=0.0;
                 double msd_x=0.0;
                 double msd_y=0.0;
                 int Nn=0;
                 int kk = 0;
                 for (kk = 0; kk < s_T->trackDuration-delta; kk++)
                 {
                        Nn++;

                        dir extreme1;
                        dir point1;
                        dir extreme2;
                        dir point2;

                        dir normalizedOrientation1;
                        normalizedOrientation1.x=trackTable[i][delta+kk].normalized_orientation.x;
                        normalizedOrientation1.y=trackTable[i][delta+kk].normalized_orientation.y;

                        dir normalizedOrientation2;
                        normalizedOrientation2.x=trackTable[i][kk].normalized_orientation.x;
                        normalizedOrientation2.y=trackTable[i][kk].normalized_orientation.y;

                        extreme1.x =  trackTable[i][delta+kk].x - 0.5*normalizedOrientation1.x*trackTable[i][delta+kk].length; // left pole coordinate
                        extreme1.y =  trackTable[i][delta+kk].y - 0.5*normalizedOrientation1.y*trackTable[i][delta+kk].length; // left pole coordinate
                        point1.x = extreme1.x + fraction*normalizedOrientation1.x*trackTable[i][delta+kk].length;
                        point1.y = extreme1.y + fraction*normalizedOrientation1.y*trackTable[i][delta+kk].length;

                        extreme2.x =  trackTable[i][kk].x - 0.5*normalizedOrientation2.x*trackTable[i][kk].length; // left pole coordinate
                        extreme2.y =  trackTable[i][kk].y - 0.5*normalizedOrientation2.y*trackTable[i][kk].length; // left pole coordinate
                        point2.x = extreme2.x + fraction*normalizedOrientation2.x*trackTable[i][kk].length;
                        point2.y = extreme2.y + fraction*normalizedOrientation2.y*trackTable[i][kk].length;

			msd_x += (point2.x-point1.x)*(point2.x-point1.x);
			msd_y += (point2.y-point1.y)*(point2.y-point1.y);
                        //msd_now += (point2.x-point1.x)*(point2.x-point1.x)+(point2.y-point1.y)*(point2.y-point1.y);
                 }
                 msd_x = (msd_x*PIXELSIZE*PIXELSIZE); // in the right units
                 msd_y = (msd_y*PIXELSIZE*PIXELSIZE); // in the right units
                 msd_now = msd_x + msd_y;

		 s_T->tau_values[delta] = delta*dt;
		 s_T->msd_values_for_tau[delta] = msd_now;
		 s_T->msd_xvalues_for_tau[delta] = msd_x;
		 s_T->msd_yvalues_for_tau[delta] = msd_y;
		 s_T->msd_count_for_tau[delta] = Nn;
                 //fprintf(classFile, "%.12f %.12f %.12f %i\n", delta*dt, msd_now, s_T.average_rodlength, Nn);
         }
}

/* Function: calculateAverageMSDForType
        Calculates a weighted average MSD as function of time-interval

        Parameters: 
		outputFile - file to write results to
                typeWeLookFor - type of trajectory (diffuser, swimmer, ..)
*/

void calculateAverageMSDForType(char *outputFile, bacType typeWeLookFor)
{
	FILE* classFile = fopen(outputFile, "w");
	double dt = TIMESTEP;

	double *tau_values = (double*) malloc(sizeof(double)*(N_FRAMES));
        double *average_msd_values_for_tau = (double*) malloc(sizeof(double)*(N_FRAMES));
        double *average_msd_xvalues_for_tau = (double*) malloc(sizeof(double)*(N_FRAMES));
        double *average_msd_yvalues_for_tau = (double*) malloc(sizeof(double)*(N_FRAMES));
        double *variance_msd_values_for_tau = (double*) malloc(sizeof(double)*(N_FRAMES));
        double *variance_msd_xvalues_for_tau = (double*) malloc(sizeof(double)*(N_FRAMES));
        double *variance_msd_yvalues_for_tau = (double*) malloc(sizeof(double)*(N_FRAMES));
	
        int *total_msd_count_for_tau = (int*) malloc(sizeof(int)*(N_FRAMES));

	int k = 0;
	for(k=0; k < N_FRAMES; k++)
        {
		tau_values[k] = k*dt;
		average_msd_values_for_tau[k] = 0.0;
		average_msd_xvalues_for_tau[k] = 0.0;
		average_msd_yvalues_for_tau[k] = 0.0;
		variance_msd_values_for_tau[k] = 0.0;
		variance_msd_xvalues_for_tau[k] = 0.0;
		variance_msd_yvalues_for_tau[k] = 0.0;
		total_msd_count_for_tau[k] = 0;
	}
	
	printf("..:\n");
	int i = 0;
	for(i=0; i < N_TRACKS; i++)
        {
		tTrackDetails s_T = allTrackDetails[i];
		bacType thisType = giveKind(s_T);
		if (thisType == typeWeLookFor)
		{
			tTrackDetails s_T = allTrackDetails[i];
			calculateTranslationalDiffusionInTime(&s_T, i, 2); // msd for centre of mass
			int delta = 0;
			for(delta=1; delta < s_T.trackDuration; delta++)
        		{
				average_msd_values_for_tau[delta] += s_T.msd_values_for_tau[delta];	
				average_msd_xvalues_for_tau[delta] += s_T.msd_xvalues_for_tau[delta];	
				average_msd_yvalues_for_tau[delta] += s_T.msd_yvalues_for_tau[delta];
				
				// Calculating the Variance V[msd] = <msd^2>-<msd>^2, next three lines are for calculating <msd^2> (averaging takes place in the end)

				variance_msd_values_for_tau[delta] += s_T.msd_values_for_tau[delta]*s_T.msd_values_for_tau[delta];
				variance_msd_xvalues_for_tau[delta] += s_T.msd_xvalues_for_tau[delta]*s_T.msd_xvalues_for_tau[delta];
				variance_msd_yvalues_for_tau[delta] += s_T.msd_yvalues_for_tau[delta]*s_T.msd_yvalues_for_tau[delta];

				total_msd_count_for_tau[delta] += s_T.msd_count_for_tau[delta];
			}
		}
	}
	
	int delta = 0;
	for(delta=1; delta < N_FRAMES; delta++)
	{
		if (total_msd_count_for_tau[delta] > 0)
		{
			average_msd_values_for_tau[delta] /= (double)total_msd_count_for_tau[delta];
			average_msd_xvalues_for_tau[delta] /= (double)total_msd_count_for_tau[delta];
			average_msd_yvalues_for_tau[delta] /= (double)total_msd_count_for_tau[delta];

			// Calculating the Variance V[msd] = <msd^2>-<msd>^2

			variance_msd_values_for_tau[delta] /= (double)total_msd_count_for_tau[delta];
                        variance_msd_xvalues_for_tau[delta] /= (double)total_msd_count_for_tau[delta];
                        variance_msd_yvalues_for_tau[delta] /= (double)total_msd_count_for_tau[delta];
			
			variance_msd_values_for_tau[delta] -= average_msd_values_for_tau[delta]*average_msd_values_for_tau[delta];
                        variance_msd_xvalues_for_tau[delta] -= average_msd_xvalues_for_tau[delta]*average_msd_xvalues_for_tau[delta];
                        variance_msd_yvalues_for_tau[delta] -= average_msd_yvalues_for_tau[delta]*average_msd_yvalues_for_tau[delta];
		}
	}

	for(delta=0; delta < N_FRAMES; delta++)
		fprintf(classFile, "%.12f %.12f %.12f %.12f %i %.12f %.12f %.12f\n", tau_values[delta], average_msd_values_for_tau[delta], average_msd_xvalues_for_tau[delta], average_msd_yvalues_for_tau[delta], total_msd_count_for_tau[delta], variance_msd_values_for_tau[delta], variance_msd_xvalues_for_tau[delta], variance_msd_yvalues_for_tau[delta]);
        
        fclose(classFile);

	free(tau_values);
	free(average_msd_values_for_tau);
	free(average_msd_xvalues_for_tau);
	free(average_msd_yvalues_for_tau);
	free(variance_msd_values_for_tau);
	free(variance_msd_xvalues_for_tau);
	free(variance_msd_yvalues_for_tau);
	free(total_msd_count_for_tau);

}

/* Function: writeTranslationalDiffusionInTime
        Writes the MSD as function of time-interval to a file for a trajectory

        Parameters: 
                outputdir - output directory
                s_T - details of trajectory i
                i - number of the trajecory
                thisType - type of trajectory (diffuser, swimmer, ..)
                mode - select from options: 0 = for anchoring point, 1 is for point of maximum movement, 2 is for centre of mass.
*/
void writeTranslationalDiffusionInTime(char *outputdir, tTrackDetails s_T, int i, bacType thisType, int mode)
{
	double dt = TIMESTEP;
	char buffer2[1024];
	if (mode == 0)
	{
		if (thisType == pivoter2)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_pivoter2_%i.dat", outputdir, i);
		else if (thisType == rotator2)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_rotator2_%i.dat", outputdir, i);
		else if (thisType == pivoter1)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_pivoter1_%i.dat", outputdir, i);
		else if (thisType == wobbler)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_wobbler_%i.dat", outputdir, i);
		else if (thisType == rotator1)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_rotator1_%i.dat", outputdir, i);
		else if (thisType == swimmer)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_swimmer_%i.dat", outputdir, i);
		else if (thisType == diffuser2)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_diffuser2_%i.dat", outputdir, i);
		else if (thisType == diffuser1)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_diffuser1_%i.dat", outputdir, i);
		else if (thisType == ambiguous)
			snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_min_ambiguous_%i.dat", outputdir, i);
	}
	else if (mode == 1)
	{
		if (thisType == pivoter2)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_pivoter2_%i.dat", outputdir, i);
                else if (thisType == rotator2)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_rotator2_%i.dat", outputdir, i);
                else if (thisType == pivoter1)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_pivoter1_%i.dat", outputdir, i);
                else if (thisType == wobbler)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_wobbler_%i.dat", outputdir, i);
                else if (thisType == rotator1)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_rotator1_%i.dat", outputdir, i);
                else if (thisType == swimmer)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_swimmer_%i.dat", outputdir, i);
                else if (thisType == diffuser2)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_diffuser2_%i.dat", outputdir, i);
                else if (thisType == diffuser1)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_diffuser1_%i.dat", outputdir, i);
                else if (thisType == ambiguous)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_max_ambiguous_%i.dat", outputdir, i);
	}
	else if (mode == 2)
        {
                if (thisType == pivoter2)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_pivoter2_%i.dat", outputdir, i);
                else if (thisType == rotator2)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_rotator2_%i.dat", outputdir, i);
                else if (thisType == pivoter1)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_pivoter1_%i.dat", outputdir, i);
                else if (thisType == wobbler)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_wobbler_%i.dat", outputdir, i);
                else if (thisType == rotator1)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_rotator1_%i.dat", outputdir, i);
                else if (thisType == swimmer)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_swimmer_%i.dat", outputdir, i);
                else if (thisType == diffuser2)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_diffuser2_%i.dat", outputdir, i);
                else if (thisType == diffuser1)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_diffuser1_%i.dat", outputdir, i);
                else if (thisType == ambiguous)
                        snprintf(buffer2, sizeof(buffer2), "%strack_TRmsd_com_ambiguous_%i.dat", outputdir, i);
        }

	FILE* classFile = fopen(buffer2, "a");

	double fraction = 0.5;
	if (mode == 0)
		fraction = s_T.mostfirm_short_msd_coordinate; // plotting MSD of most anchored point. FIX (minor): 21-10-2017
	if (mode == 1)
		fraction = s_T.leastfirm_short_msd_coordinate; // plotting MSD of least anchored point.
	if (mode == 2)
		fraction = 0.5; // COM.

        int delta = 0;
        for (delta = 1; delta < s_T.trackDuration; delta++)
        {
                 double msd_now=0.0;
                 int Nn=0;
                 int kk = 0;
		 for (kk = 0; kk < s_T.trackDuration-delta; kk++)
                 {
			Nn++;

			dir extreme1;
                        dir point1;
                        dir extreme2;
                        dir point2;

                        dir normalizedOrientation1;
                        normalizedOrientation1.x=trackTable[i][delta+kk].normalized_orientation.x;
                        normalizedOrientation1.y=trackTable[i][delta+kk].normalized_orientation.y;

                        dir normalizedOrientation2;
                        normalizedOrientation2.x=trackTable[i][kk].normalized_orientation.x;
                        normalizedOrientation2.y=trackTable[i][kk].normalized_orientation.y;

                        extreme1.x =  trackTable[i][delta+kk].x - 0.5*normalizedOrientation1.x*trackTable[i][delta+kk].length; // left pole coordinate
                        extreme1.y =  trackTable[i][delta+kk].y - 0.5*normalizedOrientation1.y*trackTable[i][delta+kk].length; // left pole coordinate
                        point1.x = extreme1.x + fraction*normalizedOrientation1.x*trackTable[i][delta+kk].length;
                        point1.y = extreme1.y + fraction*normalizedOrientation1.y*trackTable[i][delta+kk].length;

                        extreme2.x =  trackTable[i][kk].x - 0.5*normalizedOrientation2.x*trackTable[i][kk].length; // left pole coordinate
                        extreme2.y =  trackTable[i][kk].y - 0.5*normalizedOrientation2.y*trackTable[i][kk].length; // left pole coordinate
                        point2.x = extreme2.x + fraction*normalizedOrientation2.x*trackTable[i][kk].length;
                        point2.y = extreme2.y + fraction*normalizedOrientation2.y*trackTable[i][kk].length;

                        msd_now += (point2.x-point1.x)*(point2.x-point1.x)+(point2.y-point1.y)*(point2.y-point1.y);
                 }
                 msd_now = (msd_now*PIXELSIZE*PIXELSIZE)/(double)Nn; // in the right units
                 fprintf(classFile, "%.12f %.12f %.12f %i\n", delta*dt, msd_now, s_T.average_rodlength, Nn);
         }
         fclose(classFile);

}

/* Function: writeCharacteristicsToFile
        Append properties of this trajectory to files

        Parameters: 
		
                outputdir - output directory
                thisType - type of trajectory (diffuser, swimmer, ..)
                s_T - details of trajectory i
                trackTable - table containing all trajectories
                i - number of trajectory
*/
void writeCharacteristicsToFile(char *outputdir, bacType thisType, tTrackDetails s_T, tVector **trackTable, int i)
{
         char buffer4[1024];
         char buffer9[1024];

         if (thisType == rotator2)
         {
          	 snprintf(buffer4, sizeof(buffer4), "%sslopes_rotators2.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_rotators2.dat", outputdir);
         }
         else if (thisType == swimmer)
         {
            	 snprintf(buffer4, sizeof(buffer4), "%sslopes_swimmers.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_swimmers.dat", outputdir);
         }
         else if (thisType == diffuser2)
         {
                 snprintf(buffer4, sizeof(buffer4), "%sslopes_diffusers2.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_diffusers2.dat", outputdir);
         }
         else if (thisType == diffuser1)
         {
                 snprintf(buffer4, sizeof(buffer4), "%sslopes_diffusers1.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_diffusers1.dat", outputdir);
         }
         else if (thisType == wobbler)
         {
                 snprintf(buffer4, sizeof(buffer4), "%sslopes_wobblers.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_wobblers.dat", outputdir);
         }
         else if (thisType == ambiguous)
         {
                 snprintf(buffer4, sizeof(buffer4), "%sslopes_ambiguous.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_ambiguous.dat", outputdir);
         }
         else if (thisType == pivoter2)
         {
		 snprintf(buffer4, sizeof(buffer4), "%sslopes_pivoters2.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_pivoters2.dat", outputdir);
	 }
	 else if (thisType == pivoter1)
         {
                 snprintf(buffer4, sizeof(buffer4), "%sslopes_pivoters1.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_pivoters1.dat", outputdir);
	 }
	 else if (thisType == rotator1)
         {
                 snprintf(buffer4, sizeof(buffer4), "%sslopes_rotators1.dat", outputdir);
		 snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_rotators1.dat", outputdir);
         }
         else
         {
                printf("How can I write the output if no type is defined? There might be a bug in the code.");
                exit(666);
	 }

	 int trackDuration = s_T.trackDuration;

	 int midpoint = NOBINS/2;

	 double trans_average = s_T.trans_list[midpoint];
	 
	 int ttt = 0;
	 double min_trans = 1000000000000000.0;
	 double max_trans = 1000000000000000.0;

	 for (ttt = 0; ttt < NOBINS; ttt++)
	 {
	 	if (s_T.peak_trans_list[ttt] < max_trans) // taking the minimum out of the list of peaks for this p
			max_trans = s_T.peak_trans_list[ttt];
		if (s_T.min_trans_list[ttt] < min_trans) // taking the minimum out of the list of minima for this p
			min_trans = s_T.min_trans_list[ttt];
	 }

	 FILE* classFile = fopen(buffer9, "a");
	 fprintf(classFile, "%.12f %.12f %.12f %.12f %i %i\n", s_T.rot_average2, s_T.mostfirm_short_msd_value, s_T.mostfirm_short_msd_coordinate, s_T.average_rodlength, trackDuration, i);
	 fclose(classFile);

	 classFile = fopen(buffer4, "a");

	 // watch out: no newline:
	 fprintf(classFile, "%.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %.12f %i %i", s_T.short_centre_translation_slope, s_T.short_centre_translational_exponent, s_T.short_anchor_translation_slope, s_T.short_anchor_translational_exponent, s_T.long_centre_translation_slope, s_T.long_centre_translational_exponent, s_T.long_anchor_translation_slope, s_T.long_anchor_translational_exponent, s_T.short_rotation_slope, s_T.short_rotational_exponent, s_T.long_rotation_slope, s_T.long_rotational_exponent, trans_average, min_trans, max_trans, s_T.mostfirm_short_msd_value, s_T.timewise_mostfirm_msd_value, s_T.timewise_mostfirm_msd_timepoint, s_T.rot_average2, s_T.average_rodlength, trackDuration, i);
	
	 // writing time info for all translational and rotational exponents

	 int it=0;
	 int limit = 2*MINANB;
	 fprintf(classFile, " %i", limit-2);
	
	 for (it = 2; it < limit; it++)
	 {
		if (it < s_T.trackDuration)
			fprintf(classFile, " %.12f", s_T.tau_array_log_trans_slopes[it]);
		else 	
			fprintf(classFile, " %.12f", -10.0);
         }
	 for (it = 2; it < limit; it++)
         {
		if (it < s_T.trackDuration)
			fprintf(classFile, " %.12f", s_T.tau_array_log_rot_slopes[it]);
		else 	
			fprintf(classFile, " %.12f", -10.0);
         }

	 fprintf(classFile, "\n");
         fclose(classFile);
}

/* Function: flipOrientations
        flips orientations within a trajectory where required

        Parameters: 
                
                i - number of trajectory
                trackDuration - length of the trajectory
*/
void flipOrientations(int i, int trackDuration)
{
	int j = 0;
	for(j=1; j < trackDuration; j++) // start going through track i
	{
		dir Q;
		dir R;
		dir S;

		Q.x  = trackTable[i][j-1].normalized_orientation.x;
		Q.y  = trackTable[i][j-1].normalized_orientation.y;

		R.x  = trackTable[i][j].normalized_orientation.x;
		R.y  = trackTable[i][j].normalized_orientation.y;

		S.x  = -1.0*trackTable[i][j].normalized_orientation.x; // inverted
		S.y  = -1.0*trackTable[i][j].normalized_orientation.y; // inverted

		double d1 = dot(Q,R);
		double d2 = dot(Q,S);

		if (d1 > 1.0) // due to rounding errors this can be e.g. 1.000000001, then acos would go wrong later.
			d1 = 1.0;
		if (d2 > 1.0)
			d2 = 1.0;
		if (d1 < -1.0)
			d1 = -1.0;
		if (d2 < -1.0)
			d2 = -1.0;

		double angle1 = acos(d1); // result of acos is always positive
		double angle2 = acos(d2);

		if (angle2 < angle1) // flip orientation around:
		{
			trackTable[i][j].normalized_orientation.x=-1.0*trackTable[i][j].normalized_orientation.x; // flipping the vector!!
			trackTable[i][j].normalized_orientation.y=-1.0*trackTable[i][j].normalized_orientation.y; // flipping the vector!!
			trackTable[i][j].orientation.x=-1.0*trackTable[i][j].orientation.x; // flipping the vector!!
                        trackTable[i][j].orientation.y=-1.0*trackTable[i][j].orientation.y; // flipping the vector!!
                }
	}
}

/* Function: calctranslist
        calculate speeds for different points on length axis

        Parameters: 
                
                i - number of trajectory
                trackDuration - length of the trajectory
                delta - interval in frames
		trans_list - list of AVERAGE speed as function of coordinate p along length axis
		peak_trans_list - list of MAXIMUM in speed as function of coordinate p along length axis
		min_trans_list - list of MINIMUM in speed as function of coordinate p along length axis
*/
void calctranslist(int i, int trackDuration, int delta, double* trans_list, double* peak_trans_list, double* min_trans_list)
{
	int kk=0;
        int Nn=0;

        int p=0;
        for (p=0; p <= NOBINS; p++)
	{
                trans_list[p] = 0.0;
		peak_trans_list[p] = 0.0;
		min_trans_list[p] = 1000000000000000.0;
	}

        for (kk = 0; kk < (trackDuration-delta); kk++)
        {
                Nn++; // count number of points

                for (p=0; p <= NOBINS; p++)
                {
                        double fraction = p/(double)NOBINS;

                        dir extreme1;
                        dir point1;
                        dir extreme2;
                        dir point2;

                        dir normalizedOrientation1;
                        normalizedOrientation1.x=trackTable[i][delta+kk].normalized_orientation.x;
                        normalizedOrientation1.y=trackTable[i][delta+kk].normalized_orientation.y;

                        dir normalizedOrientation2;
                        normalizedOrientation2.x=trackTable[i][kk].normalized_orientation.x;
                        normalizedOrientation2.y=trackTable[i][kk].normalized_orientation.y;

                        extreme1.x =  trackTable[i][delta+kk].x - 0.5*normalizedOrientation1.x*trackTable[i][delta+kk].length; // left pole coordinate
                        extreme1.y =  trackTable[i][delta+kk].y - 0.5*normalizedOrientation1.y*trackTable[i][delta+kk].length; // left pole coordinate
                        point1.x = extreme1.x + fraction*normalizedOrientation1.x*trackTable[i][delta+kk].length;
                        point1.y = extreme1.y + fraction*normalizedOrientation1.y*trackTable[i][delta+kk].length;

                        extreme2.x =  trackTable[i][kk].x - 0.5*normalizedOrientation2.x*trackTable[i][kk].length; // left pole coordinate
                        extreme2.y =  trackTable[i][kk].y - 0.5*normalizedOrientation2.y*trackTable[i][kk].length; // left pole coordinate
                        point2.x = extreme2.x + fraction*normalizedOrientation2.x*trackTable[i][kk].length;
                        point2.y = extreme2.y + fraction*normalizedOrientation2.y*trackTable[i][kk].length;

			double thisvalue = sqrt( (point2.x-point1.x)*(point2.x-point1.x)+(point2.y-point1.y)*(point2.y-point1.y) );
                        trans_list[p] += thisvalue;

			if (thisvalue > peak_trans_list[p])
				peak_trans_list[p] = thisvalue;
			if (thisvalue < min_trans_list[p])
				min_trans_list[p] = thisvalue;
                 }
        }

	double dt = TIMESTEP;
        for (p=0; p <= NOBINS; p++)
	{
                trans_list[p] = (trans_list[p]*PIXELSIZE) / (Nn*delta*dt);
		peak_trans_list[p] = (peak_trans_list[p]*PIXELSIZE) / (delta*dt);
		min_trans_list[p] = (min_trans_list[p]*PIXELSIZE) / (delta*dt);
	}
}

/* Function: calcmsdlist
        calculate MSD for different points on length axis

        Parameters: 
                
                i - number of trajectory
                trackDuration - length of the trajectory
                delta - interval in frames
                msd_short_list - list of AVERAGE MSD as function of coordinate p along length axis
*/
void calcmsdlist(int i, int trackDuration, int delta, double* msd_short_list)
{
	int kk=0;
	int Nn=0;
	
	int p=0;
	for (p=0; p <= NOBINS; p++)
                msd_short_list[p] = 0.0;

	for (kk = 0; kk < (trackDuration-delta); kk++)
	{
		Nn++; // count number of points

	        for (p=0; p <= NOBINS; p++)
	        {
			double fraction = p/(double)NOBINS;

			dir extreme1;
			dir point1;
			dir extreme2;
			dir point2;

			dir normalizedOrientation1;
			normalizedOrientation1.x=trackTable[i][delta+kk].normalized_orientation.x;
			normalizedOrientation1.y=trackTable[i][delta+kk].normalized_orientation.y;

			dir normalizedOrientation2;
			normalizedOrientation2.x=trackTable[i][kk].normalized_orientation.x;
			normalizedOrientation2.y=trackTable[i][kk].normalized_orientation.y;

			extreme1.x =  trackTable[i][delta+kk].x - 0.5*normalizedOrientation1.x*trackTable[i][delta+kk].length; // left pole coordinate
			extreme1.y =  trackTable[i][delta+kk].y - 0.5*normalizedOrientation1.y*trackTable[i][delta+kk].length; // left pole coordinate
			point1.x = extreme1.x + fraction*normalizedOrientation1.x*trackTable[i][delta+kk].length;
			point1.y = extreme1.y + fraction*normalizedOrientation1.y*trackTable[i][delta+kk].length;

			extreme2.x =  trackTable[i][kk].x - 0.5*normalizedOrientation2.x*trackTable[i][kk].length; // left pole coordinate
			extreme2.y =  trackTable[i][kk].y - 0.5*normalizedOrientation2.y*trackTable[i][kk].length; // left pole coordinate
			point2.x = extreme2.x + fraction*normalizedOrientation2.x*trackTable[i][kk].length;
			point2.y = extreme2.y + fraction*normalizedOrientation2.y*trackTable[i][kk].length;

			msd_short_list[p] += (point2.x-point1.x)*(point2.x-point1.x)+(point2.y-point1.y)*(point2.y-point1.y);
        	 }
	}

	for (p=0; p <= NOBINS; p++)
        	msd_short_list[p] = (msd_short_list[p]*PIXELSIZE*PIXELSIZE) / (double)Nn; // not average over time
}

/* Function: calcAnchorPoints
        Calculate the anchoring point (point of least motion) on length axis of bacterium. Note that anchoring point here is defined from 0.0 to 1.0, while in the paper it is shifted to be from -0.5 to 0.5.

        Parameters: 
                
                i - number of trajectory
                s_T - details of trajectory i, containing also msd at different times scales for different coordinates

*/
void calcAnchorPoints(int i, tTrackDetails *s_T)
{
	double minvalue =1000000000000000000.0;
	double minco =1.0;
	double maxvalue =0.0000;
	double maxco =1.0;

	double short_minvalue =1000000000000000000.0;
	double short_minco =1.0;
	double short_maxvalue =0.0000;
	double short_maxco =1.0;

	int p = 0;
	for (p = 0; p <= NOBINS; p++)
	{
		double fraction = p/(double)NOBINS;
		if (s_T->msd_long_list[p] < minvalue)
		{
			minvalue = s_T->msd_long_list[p];
			minco = fraction;
		}
		if (s_T->msd_long_list[p] > maxvalue)
		{
			maxvalue = s_T->msd_long_list[p];
			maxco = fraction;
		}
		if (s_T->msd_short_list[p] < short_minvalue)
		{
			short_minvalue = s_T->msd_short_list[p];
			short_minco = fraction;
		}
		if (s_T->msd_short_list[p] > short_maxvalue)
		{
			short_maxvalue = s_T->msd_short_list[p];
			short_maxco = fraction;
		}
	}

	s_T->mostfirm_long_msd_value=minvalue;
	s_T->mostfirm_long_msd_coordinate=minco;
	s_T->leastfirm_long_msd_value=maxvalue;
	s_T->leastfirm_long_msd_coordinate=maxco;

	s_T->mostfirm_short_msd_value=short_minvalue;
	s_T->mostfirm_short_msd_coordinate=short_minco;
	s_T->leastfirm_short_msd_value=short_maxvalue;
	s_T->leastfirm_short_msd_coordinate=short_maxco;
}

/* Function: defineRodProperties
	Defines and sets some properties of a trajectory

        Parameters: 
                
                i - number of trajectory
                s_T - details of trajectory i
*/
void defineRodProperties(int i, tTrackDetails *s_T)
{

	double average_rodlength=0.0;
	double max_rodlength=0.0;
	int j = 0;
	int npoints = 0;

	for(j=0; j < s_T->trackDuration; j++) // start going through track i
        {
                // same end of the rod pol1. for two different times:
		npoints++;

                if (trackTable[i][j].length > max_rodlength)
                        max_rodlength = trackTable[i][j].length;
	
     	        dir normalizedOrientation;
                normalizedOrientation.x=trackTable[i][j].normalized_orientation.x;
                normalizedOrientation.y=trackTable[i][j].normalized_orientation.y;

		trackTable[i][j].pol1.x = trackTable[i][j].x + (0.5*normalizedOrientation.x*trackTable[i][j].length);
                trackTable[i][j].pol1.y = trackTable[i][j].y + (0.5*normalizedOrientation.y*trackTable[i][j].length);
                trackTable[i][j].pol2.x = trackTable[i][j].x - (0.5*normalizedOrientation.x*trackTable[i][j].length);
                trackTable[i][j].pol2.y = trackTable[i][j].y - (0.5*normalizedOrientation.y*trackTable[i][j].length);
			
                average_rodlength += trackTable[i][j].length;
		
        }

	average_rodlength = average_rodlength / (double)npoints;
        average_rodlength = average_rodlength*PIXELSIZE;
	s_T->average_rodlength = average_rodlength;
	s_T->max_rodlength = max_rodlength*PIXELSIZE;
}

/* Function: initialiseTrackDetails
        Initialises track details

        Parameters: 
                
                i - number of trajectory
                s_T - details of trajectory i
*/
void initialiseTrackDetails(int i, tTrackDetails *s_T)
{
        s_T->tau_short = TAUSHORT;
        s_T->tau_long = TAULONG;
    
	s_T->average_rodlength=0.0;
	s_T->max_rodlength=0.0;
	s_T->mostfirm_long_msd_value=0.0;
	s_T->mostfirm_long_msd_coordinate=0.0;
	s_T->leastfirm_long_msd_value=0.0;
	s_T->leastfirm_long_msd_coordinate=0.0;
	s_T->mostfirm_short_msd_value=0.0;
	s_T->mostfirm_short_msd_coordinate=0.0;
	s_T->leastfirm_short_msd_value=0.0;
    	s_T->leastfirm_short_msd_coordinate=0.0;

	s_T->tau_array_trans_slopes = NULL;
	s_T->tau_array_log_trans_slopes = NULL;
	s_T->tau_array_rot_slopes = NULL;
	s_T->tau_array_log_rot_slopes = NULL;
	
	s_T->tau_values = NULL;
	s_T->msd_values_for_tau = NULL;
	s_T->msd_count_for_tau = NULL;
	s_T->thisType = undefined;

        int p = 0;

        for (p=0; p <= NOBINS; p++)
        {
                s_T->msd_short_list[p] = 0.0;
                s_T->msd_long_list[p] = 0.0;
                s_T->trans_list[p] = 0.0;
                s_T->peak_trans_list[p] = 0.0;
                s_T->min_trans_list[p] = 0.0;
        }
}

/* Function: analyze_details
        Analysing trajectory

        Parameters:              
                i - number of trajectory

	Returns:
                s_T - details of trajectory i
*/
tTrackDetails analyze_details(int i)
{
	tTrackDetails s_T;
	initialiseTrackDetails(i, &s_T);

	int trackDuration = trackDetails[i][0];
	s_T.trackDuration = trackDuration;
	int firstFrame = trackDetails[i][1];
	s_T.firstFrame = firstFrame;

        if (trackDuration == 0)
        {
                printf("A track of length 0 is not possible!");
                exit(666);
        }

	double msd_short_list[NOBINSRESERVE];
	double msd_long_list[NOBINSRESERVE];
	double trans_list[NOBINSRESERVE];
	double peak_trans_list[NOBINSRESERVE];
        double min_trans_list[NOBINSRESERVE];

	int p = 0;       
 
	/////////////////////////////////////

	flipOrientations(i, trackDuration);
	defineRodProperties(i, &s_T);
	
	/////////////////////////////////////

	if (s_T.trackDuration >= MINANA)
        {

		calctranslist(i, trackDuration, s_T.tau_short, trans_list, peak_trans_list, min_trans_list);
		for (p=0; p <= NOBINS; p++)
		{
			s_T.trans_list[p] = trans_list[p];       
			s_T.peak_trans_list[p] = peak_trans_list[p];
			s_T.min_trans_list[p] = min_trans_list[p];			
		}
		calcmsdlist(i, trackDuration, s_T.tau_short, msd_short_list);
		
		for (p=0; p <= NOBINS; p++)
			 s_T.msd_short_list[p] = msd_short_list[p];

		if (trackDuration >= MINANB)
		{
			calcmsdlist(i, trackDuration, s_T.tau_long, msd_long_list);	
			for (p=0; p <= NOBINS; p++)
				s_T.msd_long_list[p] = msd_long_list[p];
		}

		// calculate the anchoring points:
		calcAnchorPoints(i, &s_T);

		double* rot_result = getRotationSlope(i, &s_T, s_T.tau_short, s_T.tau_long);
		s_T.long_rotation_slope = rot_result[0]; 
		s_T.long_rotational_exponent = rot_result[1]; 
		s_T.short_rotation_slope = rot_result[2]; 
		s_T.short_rotational_exponent = rot_result[3]; // used in giveKind 
		free(rot_result);

		// calculation at centre-of-mass:
		double* result = getTranslationSlope(i, &s_T, TAUSHORT, TAULONG, -1.0); // passing -1.0 is faster way of calculating at coordinate 0.5 (see routine for details)
		//printf("%f %f %f %f\n", result[0], result[1], result[4], result[5]);
		s_T.short_centre_translation_slope = result[0];
		s_T.short_centre_translational_exponent = result[1];
		s_T.long_centre_translation_slope = result[4];
		s_T.long_centre_translational_exponent = result[5];
		free(result);
		
		// calculation at short-time anchoring coordinate:
		result = getTranslationSlope(i, &s_T, s_T.tau_short, s_T.tau_long, s_T.mostfirm_short_msd_coordinate);
		s_T.short_anchor_translation_slope = result[0]; 
		s_T.short_anchor_translational_exponent = result[1]; // used in giveKind
		s_T.timewise_mostfirm_msd_value = result[2];
		s_T.timewise_mostfirm_msd_timepoint = result[3];
		s_T.long_anchor_translation_slope = result[4];
		s_T.long_anchor_translational_exponent = result[5]; // used in giveKind
		free(result);
	}

	return s_T; // returning object with all the details about track i
}

/* Function: writeFileHeaders
        Writes headers of files

        Parameters:              
                outputdir - output directory
                N_FRAMES - number of frames
*/
void writeFileHeaders(char *outputdir, int N_FRAMES)
{
	char buffer[1024];
	FILE* file;
	
	int total = N_ROTATORS2 + N_SWIMMERS + N_WOBBLERS + N_AMBIGUOUS + N_PIVOTERS2 + N_ROTATORS1 + N_PIVOTERS1 + N_DIFFUSERS2 + N_DIFFUSERS1 + N_UNDEFINED; // also undefined tracks are written now
	
	snprintf(buffer, sizeof(buffer), "%stracks_ALLCATS.dat", outputdir);
	file = fopen(buffer, "w");
	fprintf(file, "&%i &%i %f\n", total, N_FRAMES, 1.0);
	fclose(file);	
}

/* Function: preprocess_tracks
        Preprocesses the tracks, and gathers info to write to file headers
*/
void preprocess_tracks()
{
        printf("Pre-processing %i tracks \n", N_TRACKS); 
        int i;

   	for(i=0; i < N_TRACKS; i++)
	{
		tTrackDetails s_T = analyze_details(i);
		allTrackDetails[i] = s_T;
		classify(s_T);
	}
	writeFileHeaders(OUTPUTDIR, N_FRAMES);
	printf("Done..\n");
}

/* Function: sortFloat
        Sorts a list of floats

	Parameters:
		number - list of floats
		n - length of number list
*/
void sortFloat(double* number, int n)
{

     	/*Sort the given array number, of length n*/     

    	int j,i;
    	double temp;

    	for(i=1;i<n;i++)
    	{
 		for(j=0;j<n-i;j++)
        	{
        		if(number[j] >number[j+1])
                	{
                    		temp=number[j];
                    		number[j]=number[j+1];
                    		number[j+1]=temp;
                	}
        	}
    	}
}

/* Function: calculateSwimmingSpeedInTime
	Calculates the swimming speed for a given time interval. Smoothening applied on the data.

        Parameters: 
                outputdir - output directory
                s_T - details of trajectory i
                i - number of the trajecory
                bacType - type of trajectory (diffuser, swimmer, ..)
*/
void calculateSwimmingSpeedInTime(char *outputdir, tTrackDetails s_T, int i, bacType thisType)
{
	int span = SPAN;
	double dt = TIMESTEP;
        //int span = 4;
	int this_tau = s_T.tau_short; // hack 21 august 2017
	this_tau = 1; // hack 21 august 2017
        int j = 0;
        for(j=this_tau+span; j < (s_T.trackDuration-span); j++) // start going through track i
        {
                double *speeds= (double*) malloc(sizeof(double)*(1+2*span));
                int k = 0;
                int count=0;
                for (k = j-span; k < j+span+1; k++)
                {
                        dir v;
                        v.x = trackTable[i][k].x-trackTable[i][k-this_tau].x;
                        v.y = trackTable[i][k].y-trackTable[i][k-this_tau].y;
                        speeds[count] = sqrt(v.x*v.x+v.y*v.y);
                        count++;
                }

                double swimmingSpeed = 0.0;

                for (k = 0; k < count; k++)
                        swimmingSpeed += speeds[k];

                swimmingSpeed = swimmingSpeed/(double)(count);
                swimmingSpeed = (swimmingSpeed*PIXELSIZE)/(this_tau*dt); // in um/s

                int coordinate = (int)(j-0.5*this_tau); // shift coordinates to account for asymmetry in smoothening
                trackTable[i][coordinate].swimmingSpeed = swimmingSpeed;

                free(speeds);
        }
}

/* Function: writeRotationalDiffusionInTime
        Writes the MSOD as function of time-interval to a file for a trajectory

        Parameters: 
                outputdir - output directory
                s_T - details of trajectory i
                i - number of the trajecory
                bacType - type of trajectory (diffuser, swimmer, ..)
*/
void writeRotationalDiffusionInTime(char *outputdir, tTrackDetails s_T, int i, bacType thisType)
{
	double dt = TIMESTEP;
	char buffer2[1024];
	if (thisType == pivoter2)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_pivoter2_%i.dat", outputdir, i);
	else if (thisType == rotator2)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_rotator2_%i.dat", outputdir, i);
	else if (thisType == pivoter1)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_pivoter1_%i.dat", outputdir, i);
	else if (thisType == wobbler)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_wobbler_%i.dat", outputdir, i);
	else if (thisType == rotator1)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_rotator1_%i.dat", outputdir, i);
	else if (thisType == swimmer)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_swimmer_%i.dat", outputdir, i);
	else if (thisType == diffuser2)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_diffuser2_%i.dat", outputdir, i);
	else if (thisType == diffuser1)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_diffuser1_%i.dat", outputdir, i);
	else if (thisType == ambiguous)
		snprintf(buffer2, sizeof(buffer2), "%strack_ORmsd_ambiguous_%i.dat", outputdir, i);

	FILE* classFile = fopen(buffer2, "w");

	int* counters= (int*) malloc(sizeof(int)*s_T.trackDuration);
	double* msd_or= (double*) malloc(sizeof(double)*s_T.trackDuration);

	int mmm = 0;
	for (mmm = 0; mmm< s_T.trackDuration; mmm++)
	{
		counters[mmm] = 0;
		msd_or[mmm] = 0.0;
	}
	
	int j = 0;
	int kkk = 0;
	for (kkk = 0; kkk < s_T.trackDuration-1; kkk++) // starting point of the track 
	{	
		double tot_angle = 0.0;
		int dist = 0;
                for(j=kkk; j < (s_T.trackDuration-1); j++) // start going from starting point to end point, effectively meeting different distances
                {
			dist++;
			tot_angle += calcAngle(i, j, j+1);
			msd_or[dist] += tot_angle*tot_angle;
			counters[dist] += 1;
		}
	}

	for (j = 1; j < s_T.trackDuration; j++)
	{
		if (counters[j] > 0)
		{
			msd_or[j] = msd_or[j]/( (double)counters[j] );
			fprintf(classFile, "%.12f %.12f %.12f %i\n", j*dt, msd_or[j], s_T.average_rodlength, counters[j]);
		}
	}
	fclose(classFile);
	free(counters);
	free(msd_or);
}

/* Function: writeRotationalMotionInTime
        Writes all rotational motion invervals to an output file

        Parameters: 
                outputdir - output directory
                s_T - details of trajectory i
                i - number of the trajecory
                bacType - type of trajectory (diffuser, swimmer, ..)
*/
void writeRotationalMotionInTime(char *outputdir, tTrackDetails s_T, int i, bacType thisType)
{
        double dt = TIMESTEP;
        char buffer2[1024];

        if (thisType == pivoter2)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_pivoter2_%i.dat", outputdir, i);
        else if (thisType == rotator2)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_rotator2_%i.dat", outputdir, i);
        else if (thisType == pivoter1)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_pivoter1_%i.dat", outputdir, i);
        else if (thisType == wobbler)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_wobbler_%i.dat", outputdir, i);
        else if (thisType == rotator1)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_rotator1_%i.dat", outputdir, i);
        else if (thisType == swimmer)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_swimmer_%i.dat", outputdir, i);
        else if (thisType == diffuser2)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_diffuser2_%i.dat", outputdir, i);
        else if (thisType == diffuser1)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_diffuser1_%i.dat", outputdir, i);
        else if (thisType == ambiguous)
                snprintf(buffer2, sizeof(buffer2), "%strack_RM_ambiguous_%i.dat", outputdir, i);

        FILE* classFile = fopen(buffer2, "w");

	int j = 0;
	for(j=0; j < (s_T.trackDuration-1); j++)
	{
		double this_angle = calcAngle(i, j, j+1);
                fprintf(classFile, "%i %i %.12f %.12f\n", j, j+1, dt, this_angle);
	}

        fclose(classFile);
}

/* Function: writeOpticalLengthInTime
        Writes the fitted length in time to a file for a trajectory

        Parameters: 
                outputdir - output directory
                s_T - details of trajectory i
                i - number of the trajecory
                bacType - type of trajectory (diffuser, swimmer, ..)
*/
void writeOpticalLengthInTime(char *outputdir, int i, tTrackDetails s_T, bacType thisType)
{

	char buffer2[1024];

	if (thisType == pivoter2)
		snprintf(buffer2, sizeof(buffer2), "pivoter2");
	else if (thisType == rotator2)
		snprintf(buffer2, sizeof(buffer2), "rotator2");
	else if (thisType == pivoter1)
		snprintf(buffer2, sizeof(buffer2), "pivoter1");
	else if (thisType == wobbler)
		snprintf(buffer2, sizeof(buffer2), "wobbler");
	else if (thisType == rotator1)
		snprintf(buffer2, sizeof(buffer2), "rotator1");
	else if (thisType == swimmer)
		snprintf(buffer2, sizeof(buffer2), "swimmer");
	else if (thisType == diffuser2)
		snprintf(buffer2, sizeof(buffer2), "diffuser2");
	else if (thisType == diffuser1)
		snprintf(buffer2, sizeof(buffer2), "diffuser1");
	else if (thisType == ambiguous)
		snprintf(buffer2, sizeof(buffer2), "ambiguous");

	char buffer[1024];

	double min_length = 1000.0;
	double max_length = 0.0;
	double mean_length = 0.0;
	//double median_length = 0.0;

	snprintf(buffer, sizeof(buffer), "%soptical_length_%s_%i.dat", outputdir, buffer2, i);

	FILE* classFile = fopen(buffer, "w");
	int j = 0;

	double *temp= (double*) malloc(sizeof(double)*s_T.trackDuration);
	for(j=0; j < (s_T.trackDuration); j++) // start going through track i
	{
		fprintf(classFile, "%i %f %f %f %f %f %f\n", s_T.firstFrame+j, trackTable[i][j].length*PIXELSIZE, trackTable[i][j].x, trackTable[i][j].y, trackTable[i][j].normalized_orientation.x, trackTable[i][j].normalized_orientation.y, trackTable[i][j].width*PIXELSIZE);

		if ( (trackTable[i][j].length*PIXELSIZE) > max_length )
			max_length = trackTable[i][j].length*PIXELSIZE;
		if ( (trackTable[i][j].length*PIXELSIZE) < min_length )
			min_length = trackTable[i][j].length*PIXELSIZE;
		mean_length += trackTable[i][j].length*PIXELSIZE;
		temp[j] = trackTable[i][j].length*PIXELSIZE;
	}
	sortFloat(temp, s_T.trackDuration);
	fclose(classFile);

}

/* Function: writeScope
        Writes msd for different points along the length axis to a file for a trajectory

        Parameters: 
                outputdir - output directory
                s_T - details of trajectory i
                i - number of the trajecory
                bacType - type of trajectory (diffuser, swimmer, ..)
*/
void writeScope(char *outputdir, int i, tTrackDetails s_T, bacType thisType)
{

	char buffer2[1024];
	char buffer[1024];

	if (thisType == pivoter2)
		snprintf(buffer2, sizeof(buffer2), "pivoter2");
	else if (thisType == rotator2)
		snprintf(buffer2, sizeof(buffer2), "rotator2");
	else if (thisType == pivoter1)
		snprintf(buffer2, sizeof(buffer2), "pivoter1");
	else if (thisType == wobbler)
		snprintf(buffer2, sizeof(buffer2), "wobbler");
	else if (thisType == rotator1)
		snprintf(buffer2, sizeof(buffer2), "rotator1");
	else if (thisType == swimmer)
		snprintf(buffer2, sizeof(buffer2), "swimmer");
	else if (thisType == diffuser1)
		snprintf(buffer2, sizeof(buffer2), "diffuser1");
	else if (thisType == diffuser2)
		snprintf(buffer2, sizeof(buffer2), "diffuser2");
	else if (thisType == ambiguous)
		snprintf(buffer2, sizeof(buffer2), "ambiguous");

	snprintf(buffer, sizeof(buffer), "%sshort_scope_%s_%i.dat", outputdir, buffer2, i);
	FILE* classFile = fopen(buffer, "w");
	int p = 0;
	for (p = 0; p <= NOBINS; p++)
	{
		double fraction = (double)p/(double)NOBINS;
		fprintf(classFile, "%f %.12f %f\n", fraction, s_T.msd_short_list[p], fraction*s_T.average_rodlength);
	}
	fprintf(classFile, "\n");
	fclose(classFile);
}

/* Function: writeTrackInforToFiles
        Outputs information for individual trajectories

	Parameters: 
		outputdir - output directory
		s_T - details of trajectory i
		i - number of the trajecory
		bacType - type of trajectory (diffuser, swimmer, ..)
*/
void writeTrackInfoToFiles(char *outputdir, tTrackDetails s_T, int i, bacType thisType)
{
	writeOpticalLengthInTime(outputdir, i, s_T, thisType);
	writeScope(outputdir, i, s_T, thisType);
	writeRotationalDiffusionInTime(outputdir, s_T, i, thisType);
	writeRotationalMotionInTime(outputdir, s_T, i, thisType);
	writeTranslationalDiffusionInTime(outputdir, s_T, i, thisType, 0); // msd for coordinate on length axis that moves the least
	writeTranslationalDiffusionInTime(outputdir, s_T, i, thisType, 2); // msd for centre of mass
}

void updateTable(int i, tTrackDetails s_T, bacType thisType, tVector **trackTable)
{
	int j = 0;
	for(j=0; j < s_T.trackDuration; j++) // start going through track i
		addToTable(thisType, s_T.firstFrame+j);
}

/* Function: analyze_track
	Analyzes a trajectory

	Parameters: 
		i - number of trajectory
*/
void analyze_track(int i)
{
	tTrackDetails s_T = allTrackDetails[i];
	if (s_T.trackDuration >= MINANA)
	{
		bacType thisType = giveKind(s_T);
		s_T.thisType = thisType;
		if (thisType != undefined)
		{
			updateTable(i, s_T, thisType, trackTable);
			writeCharacteristicsToFile(OUTPUTDIR, thisType, s_T, trackTable, i);
			if (WRITETRACKINFO)
				writeTrackInfoToFiles(OUTPUTDIR, s_T, i, thisType);
			
			writeTrackToFile(thisType, trackTable, s_T, i);
		}
	}
	else
	{		
		bacType thisType = giveKind(s_T);
		s_T.thisType = thisType;
		if (thisType == undefined)
	        {
			int j = 0;
			for(j=0; j < s_T.trackDuration; j++) // start going through track i
        			addToTable(thisType, s_T.firstFrame+j);
			writeTrackToFile(thisType, trackTable, s_T, i);
		}
		else
		{
			printf("This bacterium should have remained undefined because it's trajectory is too short (MINANA=%i), something went wrong\n", MINANA);
			exit(666);
		}
	}
}

/* Function: createFiles
	Creates output files
	
	Parameters:
		outputdir - output directory
*/
void createFiles(char *outputdir)
{
	printf("Creating files..\n");

	char buffer4[1024];
	char buffer9[1024];

	snprintf(buffer4, sizeof(buffer4), "%sslopes_rotators2.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_rotators2.dat", outputdir);
	FILE* testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_swimmers.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_swimmers.dat", outputdir);
	testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_diffusers1.dat", outputdir);
        snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_diffusers1.dat", outputdir);
        testFile = fopen(buffer4, "w");
        fclose(testFile);
        testFile = fopen(buffer9, "w");
        fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_diffusers2.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_diffusers2.dat", outputdir);
	testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_wobblers.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_wobblers.dat", outputdir);
	testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_ambiguous.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_ambiguous.dat", outputdir);
	testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_pivoters2.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_pivoters2.dat", outputdir);
	testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_rotators1.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_rotators1.dat", outputdir);
	testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

	snprintf(buffer4, sizeof(buffer4), "%sslopes_pivoters1.dat", outputdir);
	snprintf(buffer9, sizeof(buffer9), "%smsd_short_minscope_pivoters1.dat", outputdir);
	testFile = fopen(buffer4, "w");
	fclose(testFile);
	testFile = fopen(buffer9, "w");
	fclose(testFile);

}

/* Function: allocateArrays
	Allocates memory for bookkeeping
*/
void allocateArrays()
{
	trackTable= (tVector**) malloc(sizeof(tVector*)*N_TRACKS);
	trackDetails=(int**) malloc(sizeof(int*)*N_TRACKS);

	allTrackDetails = (tTrackDetails*) malloc(sizeof(tTrackDetails)*N_TRACKS);

	ROTATOR2_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	SWIMMER_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	WOBBLER_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	DIFFUSER2_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	DIFFUSER1_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	AMBIGUOUS_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	PIVOTER2_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	PIVOTER1_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	ROTATOR1_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);
	UNDEFINED_ARRAY = (int*) malloc(sizeof(int)*N_FRAMES);

	printf("making arrays..\n");
	int i = 0;
	for (i = 0; i < N_FRAMES; i++) // N_FRAMES comes from file containing calculated tracks
	{
		ROTATOR2_ARRAY[i] = 0;
		SWIMMER_ARRAY[i] = 0;
		WOBBLER_ARRAY[i] = 0;
		DIFFUSER2_ARRAY[i] = 0;
		DIFFUSER1_ARRAY[i] = 0;
		AMBIGUOUS_ARRAY[i] = 0;
		PIVOTER2_ARRAY[i] = 0;
		PIVOTER1_ARRAY[i] = 0;
		ROTATOR1_ARRAY[i] = 0;
		UNDEFINED_ARRAY[i] = 0;  
	}
}

/* Function: readTracks
	Reads the trajectories from a file
*/
void readTracks()
{
	printf("Loading tracks.. \n");

	FILE* fpt=fopen(TRACKINPUTFILE, "r");
	char str[256];
	mygetline(str,fpt);
	float version;
	
	sscanf(str, "&%i &%i %e", &N_TRACKS, &N_FRAMES, &version);
	if (version == 0.0)
        {
                printf("Error, undefined file format version!\n");
                exit(666);
        }

	int tmp;
	printf("Found %i tracks in %i frames\n", N_TRACKS, N_FRAMES);
	printf("File format version: %f\n", version);

	allocateArrays();

	float x;
	float y;
	float q;
	float r;
	float s;
	float ar1;
	float ar2;
	float ar3;
	float width;
	float accuracy;
	int thispart;

	int trackDuration=0;

	int i = 0;
	for (i =0; i < N_TRACKS; i++)
	{
		mygetline(str,fpt);
		sscanf(str,"&%i &%i", &tmp, &trackDuration);

		trackDetails[i]= (int*) malloc(sizeof(int)*4);
		trackDetails[i][0] = trackDuration;

		trackTable[i]= (tVector*) malloc(sizeof(tVector)*trackDuration);

		int frameNo=0;
		int k;  	
		for (k =0; k < trackDuration; k++)
		{
			mygetline(str,fpt);

			if (version > 0)
				sscanf(str,"%i %i %e %e %e %e %e %e %e %e", &frameNo, &thispart, &x, &y, &q, &r, &s, &ar1, &ar2, &ar3);	
			else if (version < 0)
				sscanf(str,"%i %i %e %e %e %e %e %e %e %e %e %e", &frameNo, &thispart, &x, &y, &q, &r, &s, &ar1, &ar2, &ar3, &width, &accuracy);	
			
			if (k == 0)
			{
				trackDetails[i][1] = frameNo;
				trackDetails[i][2] = 0; // number of New VALID tracks
				trackDetails[i][3] = 0; // number of CHOPPED tracks
			}
			
			trackTable[i][k].x= (double)x;
			trackTable[i][k].y= (double)y;
			trackTable[i][k].orientation.x= (double)q; // this should be a vector pointing from COM to center of spherocylinder cap.
			trackTable[i][k].orientation.y= (double)r;

			if (trackTable[i][k].x > rc_frameWidth || trackTable[i][k].y > rc_frameHeight || trackTable[i][k].x <= 0 || trackTable[i][k].y <= 0)
			{
				printf("Coordinate out of bounds: %f %f\n", trackTable[i][k].x, trackTable[i][k].y);
				exit(666);
			}

			dir normed;

			normed.x = trackTable[i][k].orientation.x;
			normed.y = trackTable[i][k].orientation.y;
			normed = normalizeVector(normed);

			trackTable[i][k].normalized_orientation.x = normed.x;		
			trackTable[i][k].normalized_orientation.y = normed.y;		

			trackTable[i][k].pol1.x = 0.0;
			trackTable[i][k].pol1.y = 0.0;
			trackTable[i][k].pol2.x = 0.0;
			trackTable[i][k].pol2.y = 0.0;

			trackTable[i][k].length=(double)s;	// this should be the optical length, from end to end
			trackTable[i][k].width=(double)width;	// this should be the optical length, from end to end
			trackTable[i][k].accuracy=(double)accuracy;	// this should be the accuracy of the fit, low values mean width-axis and length-axis cannot be distinguished
			trackTable[i][k].ar1=(double)ar1;
			trackTable[i][k].ar2=(double)ar2;
			trackTable[i][k].ar3=(double)ar3;
			trackTable[i][k].n=thispart;
			
			trackTable[i][k].x_cell = 0;
			trackTable[i][k].y_cell = 0;
			trackTable[i][k].neighbours = 0;

			trackTable[i][k].state = 0;
			trackTable[i][k].jump = 0;
			
			trackTable[i][k].swimmingSpeed = -10.0;
			trackTable[i][k].swimmingDirection_cor = -10.0;
		}
	}	

	fclose(fpt);
	printf("Done..\n");
}

/* Function: writeTrackTable
	Writes the number of each type in every frame to a file

	Parameters:
		outputdir - output directory
*/
void writeTrackTable(char *outputdir)
{  
	printf("Writing sorts..\n");
	char buffer[1024];
	snprintf(buffer, sizeof(buffer), "%ssorts.dat", outputdir); 
	FILE* sortFile = fopen(buffer, "w");
	fprintf(sortFile, "frame#, total, rotators(2), swimmers, wobblers, diffusers(2), diffusers(1), ambiguous, pivoters(2), pivoters(1), rotators(1), undefined\n");
	int i = 0;
	for (i = 0; i < N_FRAMES; i++) // N_FRAMES comes from file containing calculated tracks
	{
		int total = ROTATOR2_ARRAY[i] + SWIMMER_ARRAY[i] + WOBBLER_ARRAY[i] + DIFFUSER2_ARRAY[i] + DIFFUSER1_ARRAY[i] + AMBIGUOUS_ARRAY[i] + PIVOTER2_ARRAY[i] + PIVOTER1_ARRAY[i] + ROTATOR1_ARRAY[i]; // only the undefined are not included!
	   	fprintf(sortFile, "%i %i %i %i %i %i %i %i %i %i %i %i\n", i, total, ROTATOR2_ARRAY[i], SWIMMER_ARRAY[i], WOBBLER_ARRAY[i], DIFFUSER2_ARRAY[i], DIFFUSER1_ARRAY[i], AMBIGUOUS_ARRAY[i], PIVOTER2_ARRAY[i], PIVOTER1_ARRAY[i], ROTATOR1_ARRAY[i], UNDEFINED_ARRAY[i]);
   	}

   	fclose(sortFile);
   	printf("Done..\n");
}


void performExtraAnalysis(char* outputdir)
{
	char buffer[1024];
        snprintf(buffer, sizeof(buffer), "%saverage_msd_swimmers.dat", outputdir);
	bacType thisType = swimmer;
	printf("Calculating Average MSD for swimmers.. \n");
	calculateAverageMSDForType(buffer, thisType);
	printf("Done. \n");
}

/* Function: closeFiles
	Closes some files
	
	 Parameters:
                outputdir - output directory
*/
void closeFiles(char *outputdir)
{
  	printf("Closing..\n");
	char buffer[1024];
	snprintf(buffer, sizeof(buffer), "%sdetails.info", outputdir); 
	FILE* infoFile = fopen(buffer, "w");

	fprintf(infoFile, "This dataset was analysed for trajecories of rod-shaped particles \n \n");

	printf("Done..\n");

   	fclose(infoFile);
}

/* Function: errorcheck 
	Checks for errors in input parameters 
*/
void errorcheck()
{
	if (TIMESTEP < 0.000001)
	{
		printf("NO VALID TIMESTEP GIVEN\n");
                exit(915);
	}
	if (PIXELSIZE < 0.0000001)
	{
		printf("NO VALID PIXELSIZE GIVEN\n");
                exit(915);
	}
  	if (TAUSHORT >= MINANA)
  	{
		printf("tau_short can not be greater than MINANA\n");
		exit(915);
  	}
  	if (MINANA >= MINANB)
  	{
		printf("MINANA can not be greater than MINANB\n");
		exit(915);
  	}
}

/* Function: freeMemory
	Cleans up arrays in memory
*/
void freeMemory()
{
	printf("Freeing memory..\n");

        free(ROTATOR2_ARRAY);
        free(SWIMMER_ARRAY);
        free(WOBBLER_ARRAY);
        free(DIFFUSER2_ARRAY);
        free(DIFFUSER1_ARRAY);
        free(AMBIGUOUS_ARRAY);
        free(PIVOTER2_ARRAY);
        free(PIVOTER1_ARRAY);
        free(ROTATOR1_ARRAY);
        free(UNDEFINED_ARRAY);

	int i =0;
	for (i = 0; i < N_TRACKS; i++)
	{
		free(allTrackDetails[i].tau_array_trans_slopes);
		free(allTrackDetails[i].tau_array_log_trans_slopes);
		free(allTrackDetails[i].tau_array_rot_slopes);
		free(allTrackDetails[i].tau_array_log_rot_slopes);
		free(allTrackDetails[i].tau_values);
		free(allTrackDetails[i].msd_values_for_tau);
		free(allTrackDetails[i].msd_xvalues_for_tau);
		free(allTrackDetails[i].msd_yvalues_for_tau);
		free(allTrackDetails[i].msd_count_for_tau);
	}
	free(allTrackDetails);
	printf("Done..\n");
}

int main(int argc, char *argv[])
{
   	GetNameList(argc,argv);
   	PrintNameList (stdout);
 
	errorcheck();
	readTracks();
	createFiles(OUTPUTDIR);
	preprocess_tracks();

	printf("Analyzing tracks.. \n");

	int i;
	for (i = 0; i < N_TRACKS; i++) 
		analyze_track(i); 
 
	printf("Done.. \n");

	writeTrackTable(OUTPUTDIR);
	//performExtraAnalysis(OUTPUTDIR);
	freeMemory();
	closeFiles(OUTPUTDIR);

	return 0;
}
