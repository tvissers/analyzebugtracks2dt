#!/bin/bash

echo "TRACKINPUTFILE ../testcase/input/tracks_fixed.dat" > analyzeBugTracks2Dt.in
echo "OUTPUTDIR ../testcase/output/" >> analyzeBugTracks2Dt.in
echo "DEBUG 1" >> analyzeBugTracks2Dt.in
echo "PIXELSIZE 0.234" >> analyzeBugTracks2Dt.in
echo "TIMESTEP 0.0333" >> analyzeBugTracks2Dt.in
echo "MINANA 25" >> analyzeBugTracks2Dt.in
echo "MINANB 180" >> analyzeBugTracks2Dt.in
echo "TAULONG 120" >> analyzeBugTracks2Dt.in
echo "TAUSHORT 12" >> analyzeBugTracks2Dt.in
echo "LOWERBOUNDSHORTKT 0.6" >> analyzeBugTracks2Dt.in
echo "LOWERBOUNDLONGKT 0.6" >> analyzeBugTracks2Dt.in
echo "UPPERBOUNDSHORTKT 1.3" >> analyzeBugTracks2Dt.in
echo "LOWERBOUNDSHORTKR 0.3" >> analyzeBugTracks2Dt.in
echo "UPPERBOUNDSHORTKR 1.2" >> analyzeBugTracks2Dt.in
echo SPAN 1 >> analyzeBugTracks2Dt.in
echo WRITETRACKINFO 0 >> analyzeBugTracks2Dt.in
./analyzeBugTracks2Dt
