#define NP_I  ((int *)  (nameList[k].vPtr) + j)
#define NP_LLI  ((long long int *)  (nameList[k].vPtr) + j)
#define NP_R  ((float *) (nameList[k].vPtr) + j)
#define NP_C  ((char *) (nameList[k].vPtr) + j)

int GetNameList (int argc, char **argv)
{
  int j, k, match, ok;
  char buff[800], *token;
  FILE *fp;

  strcpy (buff, argv[argc-1]);
  if(argc == 1) strcat (buff, ".in");

  if ((fp = fopen (buff, "r")) == 0) return (0);
  for (k = 0; k < sizeof (nameList) / sizeof (NameList); k ++)
     nameList[k].vStatus = 0;
  ok = 1;
  while (1) {
    if(fgets (buff, 800, fp)== NULL) if(!feof(fp)) printf("Error reading parameter file\n");
    if (feof (fp)) break;
    token = strtok (buff, " \t\n");
    if (! token) break;
    match = 0;
    for (k = 0; k < sizeof (nameList) / sizeof (NameList); k ++) {
      if (strcmp (token, nameList[k].vName) == 0) {
        match = 1;
        if (nameList[k].vStatus == 0) {
          nameList[k].vStatus = 1;
          if(nameList[k].vType == N_C) {
            token = strtok (NULL, " \n");
            if (token) {
              for (j = 0; j < strlen(token);j++){
                if(j< nameList[k].vLen){
                  *NP_C=token[j];
                } else {
                  nameList[k].vStatus=3;
                  ok = 0;
                }
              }  
            } else {
              nameList[k].vStatus=3;
              ok = 0;
            }
          } else {
            for (j = 0; j < nameList[k].vLen; j ++) {
              token = strtok (NULL, ", \t\n");
              if (token) {
                switch (nameList[k].vType) {
                  case N_LLI:
                    *NP_LLI = atoll (token);
                    break;
                  case N_I:
                    *NP_I = atol (token);
                    break;
                  case N_R:
                    *NP_R = atof (token);
                    break;
                  case N_C:
                    break;
                }
              } else {
                nameList[k].vStatus = 2;
                ok = 0;
              }
            }
          }
          token = strtok (NULL, ", \t\n");
          if (token) {
                  printf("Status set to 3 string is '%s'\n", token);
            nameList[k].vStatus = 3;
            ok = 0;
          }
          break;
        } else {
          nameList[k].vStatus = 4;
          ok = 0;
        }
      }
    }
    if (! match) ok = 0;
  }
  fclose (fp);
  for (k = 0; k < sizeof (nameList) / sizeof (NameList); k ++) {
    if (nameList[k].vStatus != 1) ok = 0;
  }
  return (ok);
}

void PrintNameList (FILE *fp)
{
  int j, k;

  printf("---------------------------------------------\n");
  fprintf (fp, "NameList -- data\n");
  for (k = 0; k < sizeof (nameList) / sizeof (NameList); k ++) {
    fprintf (fp, "%s\t", nameList[k].vName);
    if (strlen (nameList[k].vName) < 8) fprintf (fp, "\t");
    if (nameList[k].vStatus > 0) {
      if(nameList[k].vType == N_C){
        j=0;
        fprintf (fp,"%s ", NP_C);
      } else 
      for (j = 0; j < nameList[k].vLen; j ++) {
        switch (nameList[k].vType) {
          case N_LLI:
            fprintf (fp, "%lld ", *NP_LLI);
            break;
          case N_I:
            fprintf (fp, "%d ", *NP_I);
            break;
          case N_R:
            fprintf (fp, "%#g ", *NP_R);
            break;
          case N_C:
            break;
        }
      }
    }
    switch (nameList[k].vStatus) {
      case 0:
      fprintf (fp, "** no data");
      break;
      case 1:
        break;
      case 2:
        fprintf (fp, "** missing data");
        break;
      case 3:
        fprintf (fp, "** extra data");
        break;
      case 4:
        fprintf (fp, "** multiply defined");
        break;
    }
    fprintf (fp, "\n");
  }
  fprintf (fp,"---------------------------------------------\n");
  fflush (fp);
}

