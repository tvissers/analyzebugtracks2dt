------------------------------------------------------------------------------
About: License

    This file is part of analyzeBugTracks2Dt.

    analyzeBugTracks2Dt is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    analyzeBugTracks2Dt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with analyzeBugTracks2Dt.  If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
About: This program

Please cite the following papers if you use this code for a scientific publication:

T. Vissers et al., Science Advances, 4, 4, eaao1170, 2018.
https://doi.org/10.1126/sciadv.aao1170

T. Vissers et al., PloS One., 14, 6, e0217823, 2019.
https://doi.org/10.1371/journal.pone.0217823

--------------------------------------------------------------------------------

About: repository

At the time of publication, a repository for this code is available at: https://git.ecdf.ed.ac.uk/tvissers/analyzeBugTracks2Dt
Updated versions may appear there.

--------------------------------------------------------------------------------

About: Running the example

Type 'make' to compile the code and create the executable.

To run the testcase a analyzeBugTracks2Dt.in should be provided with this program. This file
contains the setting used by the tracking algorithm and should look as follows:

--- text -----------------------------------------------------------------------
TRACKINPUTFILE ../testcase/input/tracks_fixed.dat
OUTPUTDIR ../testcase/output/
DEBUG 1
PIXELSIZE 0.234
TIMESTEP 0.0333
MINANA 25
MINANB 180
TAULONG 120
TAUSHORT 12
LOWERBOUNDSHORTKT 0.6
LOWERBOUNDLONGKT 0.6
UPPERBOUNDSHORTKT 1.3
LOWERBOUNDSHORTKR 0.3
UPPERBOUNDSHORTKR 1.2
SPAN 1
WRITETRACKINFO 0
--------------------------------------------------------------------------------

About: Input files

* tracks_fixed.dat file containing filtered trajectories of bacteria.

--------------------------------------------------------------------------------

About: Output files

* sorts.dat file containing number of cells with different types of dynamics, for each frame.
* tracks_ALLCATS.dat file containing all trajectories, each labelled with dynamics type in header of trajectory.
* details.info a line of info for the analysis.
* slopes files per type containing translational and rotational exponents and other information.
* minscope file per type containing dynamical information and coordinate of minimal MSD.
* register_adhering.dat shows summary of adhering cells for this image time series
* if WRITETRACKINFO is set to 1 in inputfile, output also contains info for each trajectory (warning: lots of files!).
--------------------------------------------------------------------------------

About: sorts.dat - file format

This file contains the number of cells for each identified type of dynamics. Each line gives data for one frame. 

--- text -----------------------------------------------------------------------
Note: Fields can be zero for the first N frames (typically N=100) if trackRods2Dt was configured to discard those from a movie (for some datasets where stage moves between positions the start of movies can sometimes be blurry because of movement).
--------------------------------------------------------------------------------

Data is formatted as follows:

--- text -----------------------------------------------------------------------
frame# total rotators2 (TYPE=1) swimmers (TYPE=2) wobblers (TYPE=3) diffusers2 (TYPE=4) diffusers1 (TYPE=5) ambiguous (TYPE=6) pivoters2 (TYPE = 7) pivoters1 (TYPE=8) rotators1 (TYPE = 9) undefined (TYPE = 10)
..
..
--------------------------------------------------------------------------------

--- text -----------------------------------------------------------------------
Note: the field 'total' does not include 'undefined'.
Note: types are described below
--------------------------------------------------------------------------------

Different types (as calculated in function giveKind, also see code and code documentation):

--- text -----------------------------------------------------------------------
1       Active rotator, short translational time scale shows diffuser, but long translational time scale shows adherer, rotational shows active rotator
2       Swimmer, short time scale translational shows swimmer
3       Wobbler, short time scale translational+rotational shows wobbler
4       Diffuser, short time scale translational shows diffuser, trajectory is long
5       Diffuser, short time scale translational shows diffuser, trajectory is short
6       Ambiguous, short time scale shows adherer, but trajectory is suspiciously short for adherer
7       Pivoter, short time scale translational shows diffuser, but longer translational time scale shows adherer. Rotational shows pivoter
8       Pivoter, short time scale translational+rotational shows pivoter
9       Active rotator, short time scale translational+rotational shows active rotator
10      Undefined, typically when trajectory is too short to be classified
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

About: tracks_ALLCATS.dat - file format

This file can be used to check the output of the analysis code. The format is as follows:

--- text -----------------------------------------------------------------------
&NUMBEROFTRAJECTORIES &NUMBEROFFRAMES VERSION
TRAJECTORYNO TRAJECTORYDURATION TYPE FIELD
FRAMENO PARTICLEID X_COM Y_COM X_OR_NORMALISED Y_OR_NORMALISED LENGTH COOR1 COOR2 SWIMCOR
..
..
TRAJECTORYNO TRAJECTORYDURATION TYPE FIELD
FRAMENO PARTICLEID X_COM Y_COM X_OR_NORMALISED Y_OR_NORMALISED LENGTH COOR1 COOR2 SWIMCOR
..
..
--------------------------------------------------------------------------------

Legend:

The file header looks like:

--- text -----------------------------------------------------------------------
NUMBEROFTRAJECTORIES    :       total number of trajectories
NUMBEROFFRAMES          :       total number of frames
VERSION	                :       1.000000
--------------------------------------------------------------------------------

For each trajectory, the header looks like:

--- text -----------------------------------------------------------------------
TRAJECTORYNO              :       ID of the trajectory, starts at 0
TRAJECTORYDURATION        :       Duration (in frames) of the trajectory
TYPE                	  :       TYPE (see below)
FIELD                     :       Field used for additional info
--------------------------------------------------------------------------------

For each frame inside the trajectory the following information is given:

--- text -----------------------------------------------------------------------
FRAMENO         :       frameno since start of the video
PARTICLEID      :       original ID number of rod-shaped object in the frame
X_COM           :       x position in pixels
Y_COM           :       y position in pixels
X_OR            :       normalised x-direction of bacterium
Y_OR            :       normalised y-direction of bacterium
LENGTH          :       length (in pixels)
COOR1		: 	least moving coordinate on length axis (between 0 and 1), shift with -0.5 to obtain coordinates as in paper
COOR2		: 	most moving coordinate on length axis (one of the poles, 0 or 1), shift with -0.5 to obtain coordinates as in paper
SWIMCOR		:	swimming correlation, set to -10 by default
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

About: msd_short_minscope*.dat - file format

For each type, for found trajectories is listed per trajectory:

--- text ---------------------------------------------------------------------------------------------
OR_MSD MSD_VALUE_OF_LEAST_MOVING_POINT LEAST_MOVING_COORDINATE AVERAGE_RODLENGTH TRACKDURATION TRACKNO
OR_MSD MSD_VALUE_OF_LEAST_MOVING_POINT LEAST_MOVING_COORDINATE AVERAGE_RODLENGTH TRACKDURATION TRACKNO
..
..
------------------------------------------------------------------------------------------------------

--- text ---------------------------------------------------------------------------------------------
Note: least moving coordinate is between 0 and 1, shift with -0.5 to obtain same range as in paper.
------------------------------------------------------------------------------------------------------


About: slopes_*.dat - file format

Each relevant type has a file, for which for each trajectory is listed (on a separate line for each):

--- text ---------------------------------------------------------------------------------------------
s_T.short_centre_translation_slope s_T.short_centre_translational_exponent s_T.short_anchor_translation_slope s_T.short_anchor_translational_exponent s_T.long_centre_translation_slope s_T.long_centre_translational_exponent s_T.long_anchor_translation_slope s_T.long_anchor_translational_exponent s_T.short_rotation_slope s_T.short_rotational_exponent s_T.long_rotation_slope s_T.long_rotational_exponent trans_average min_trans max_trans s_T.mostfirm_short_msd_value s_T.timewise_mostfirm_msd_value s_T.timewise_mostfirm_msd_timepoint s_T.rot_average2 s_T.average_rodlength trackDuration i [.. translational and rotational exponents for different times, see code for details.. ]
------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------

About: register_adhering.dat - file format

If type of trajectory is adhering (1, 3, 7, 8 or 9), a line will be written containing:

--- text ---------------------------------------------------------------------------------------------
i TYPE firstFrame trackDuration x_av y_av mostfirm_short_msd_coordinate leastfirm_short_msd_coordinate
--- text ---------------------------------------------------------------------------------------------

This information can be used to track adhering cells in consecutive videos.
